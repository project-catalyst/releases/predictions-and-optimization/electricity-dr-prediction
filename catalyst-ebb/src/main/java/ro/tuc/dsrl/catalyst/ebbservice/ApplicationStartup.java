package ro.tuc.dsrl.catalyst.ebbservice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.service.AsynchronousService;

import java.time.LocalDateTime;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private static final Log LOGGER = LogFactory.getLog(ApplicationStartup.class);

    private final AsynchronousService asynchronousService;

    @Value("${start.asynchronous.service}")
    private boolean startAsynchronousServiceFlag;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public ApplicationStartup(AsynchronousService asynchronousService) {
        this.asynchronousService = asynchronousService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {

        if(startAsynchronousServiceFlag){
            if(debugMode) {
                LOGGER.info("Started asynchronous prediction services");
                LOGGER.info("Current date time is: " + LocalDateTime.now());
            }
            try {
                asynchronousService.executeAsynchronously();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
