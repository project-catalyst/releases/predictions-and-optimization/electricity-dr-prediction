package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

public enum PredictionApiUrl implements ApiUrl {

    POST_NEAR_REALTIME_PREDICTION("near_real_time/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_INTRADAY_PREDICTION("intraday/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_DAYAHEAD_PREDICTION("dayahead/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_DAYAHEAD_UPPER_FLEXIBILITY("dayahead/predict_flexibility_upper_bounds/{algorithmType}/{dataScenario}/{componentType}"),
    POST_DAYAHEAD_LOWER_FLEXIBILITY("dayahead/predict_flexibility_lower_bounds/{algorithmType}/{dataScenario}/{componentType}"),

    // endpoints for load models and training in real simulated scenarios
    POST_DAYAHEAD_LOAD_MODEL_AND_PREDICT("dayahead/load_model_and_predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_INTRADAY_LOAD_MODEL_AND_PREDICT("intraday/load_model_and_predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_NEAR_REAL_TIME_LOAD_MODEL_AND_PREDICT("near_real_time/load_model_and_predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_FLEXIBILITY_UPPER_LOAD_MODEL_AND_PREDICT(""),
    POST_FLEXIBILITY_LOWER_LOAD_MODEL_AND_PREDICT("");

    private String value;

    PredictionApiUrl(String url) {
        this.value = url;
    }

    public String value() {
        return value;
    }
}
