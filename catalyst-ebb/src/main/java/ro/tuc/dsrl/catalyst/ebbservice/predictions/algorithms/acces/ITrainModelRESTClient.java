package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces;

import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.util.TrainingApi;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

public interface ITrainModelRESTClient<T, O> {

    O trainModel(T t, TrainingApi trainingApi, DataScenario dataScenario, AlgorithmType algorithmType,
                 DeviceTypeEnum componentType, String endTime);
}