package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.lstm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmRESTClient;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class LongShortTermMemoryRESTClient implements IAlgorithmRESTClient<EnergyProfileDTO, List<Double>> {

    private static final Logger LOGGER = Logger.getLogger(LongShortTermMemoryRESTClient.class.getName());

    private final RestAPIClient restAccessPoint;

    @Autowired
    public LongShortTermMemoryRESTClient(RestAPIClient restAccessPoint) {
        this.restAccessPoint = restAccessPoint;
    }

    @Override
    public List<Double> predictEnergyValues(
            EnergyProfileDTO energyProfileDTO,
            PredictionApi predictionApi,
            DataScenario dataScenario,
            AlgorithmType algorithmType,
            DeviceTypeEnum componentType,
            String startTime) {

        LOGGER.info("Call of LSTM for predicting...");

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dataScenario", dataScenario.getScenario());
        urlVariables.put("algorithmType", AlgorithmType.LSTM);
        urlVariables.put("componentType", componentType.type());
        urlVariables.put("timestamp", startTime);

        return restAccessPoint.postObject(
                predictionApi.getApiUrl(),
                energyProfileDTO,
                new ParameterizedTypeReference<List<Double>>() {},
                urlVariables);
    }
}
