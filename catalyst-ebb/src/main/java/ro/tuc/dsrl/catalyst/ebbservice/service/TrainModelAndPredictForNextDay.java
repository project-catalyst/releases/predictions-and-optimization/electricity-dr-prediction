package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.AlgorithmsBroker;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.repository.DeviceRepository;
import ro.tuc.dsrl.catalyst.ebbservice.repository.MeasurementRepository;
import ro.tuc.dsrl.catalyst.ebbservice.repository.TrainedModelRepository;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ServerRoomDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class TrainModelAndPredictForNextDay implements Runnable{


    private static final Log LOGGER = LogFactory.getLog(TrainModelAndPredictForNextDay.class);

    private final AlgorithmsBroker algorithmsBroker;
    private int intervalTimeHours;
    private String scenario;
    private String predictionType;
    private String flexibilityType;
    private String granularity;
    private LocalDateTime currentDateTime;
    private final EnergyProfileService energyProfileService;
    private LocalDateTime startTimeForPrediction;
    private boolean dayaheadDCEnergyConsumption;
    private boolean dayaheadServerEnergyConsumption;
    private boolean dayaheadRealWorkloadEnergyConsumption;
    private boolean dayaheadDelayTolerantWorkloadEnergyConsumption;
    private boolean dayaheadCoolingEnergyConsumption;
    private boolean dayaheadThermalEnergyConsumption;
    private String dataCenterName;
    private LocalDateTime previousPredictionDateTime;
    private LocalDateTime predictionDatetTime;

    @Autowired
    private final DeviceRepository deviceRepository;
    @Autowired
    private final PredictionServiceUtils predictionServiceUtils;

    @Value("${debug.mode}")
    private boolean debugMode;

    public TrainModelAndPredictForNextDay(AlgorithmsBroker algorithmsBroker,
                                          @Value("${dayahead.dc.energy.consumption}") boolean dayaheadDCEnergyConsumption,
                                          @Value("${dayahead.server.energy.consumption}") boolean dayaheadServerEnergyConsumption,
                                          @Value("${dayahead.real.workload.energy.consumption}") boolean dayaheadRealWorkloadEnergyConsumption,
                                          @Value("${dayahead.delay.tolerant.workload.energy.consumption}") boolean dayaheadDelayTolerantWorkloadEnergyConsumption,
                                          @Value("${dayahead.cooling.energy.consumption}") boolean dayaheadCoolingEnergyConsumption,
                                          @Value ("${dayahead.thermal.energy.consumption}") boolean dayaheadThermalEnergyConsumption,
                                          @Value("${prediction.type}") String predictionType,
                                          @Value("${flexibility.type}") String flexibilityType,
                                          @Value("${scenario}") String scenario,
                                          @Value("${datacenter.name}") String dataCenterName,
                                          EnergyProfileService energyProfileService,
                                          DeviceRepository deviceRepository,
                                          MeasurementRepository measurementRepository,
                                          PredictionServiceUtils predictionServiceUtils){

        this.algorithmsBroker = algorithmsBroker;
        this.dayaheadDCEnergyConsumption = dayaheadDCEnergyConsumption;
        this.dayaheadServerEnergyConsumption = dayaheadServerEnergyConsumption;
        this.dayaheadRealWorkloadEnergyConsumption = dayaheadRealWorkloadEnergyConsumption;
        this.dayaheadDelayTolerantWorkloadEnergyConsumption = dayaheadDelayTolerantWorkloadEnergyConsumption;
        this.dayaheadCoolingEnergyConsumption = dayaheadCoolingEnergyConsumption;
        this.dayaheadThermalEnergyConsumption = dayaheadThermalEnergyConsumption;
        this.intervalTimeHours = 24;
        this.predictionType = predictionType;
        this.flexibilityType = flexibilityType;
        this.scenario = scenario;
        this.dataCenterName = dataCenterName;
        this.granularity = "dayahead";
        this.currentDateTime = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0);
        this.previousPredictionDateTime = currentDateTime;
        this.energyProfileService = energyProfileService;
        this.startTimeForPrediction = currentDateTime.plusHours(intervalTimeHours);
        this.deviceRepository = deviceRepository;
        this.predictionServiceUtils = predictionServiceUtils;

    }

    @Override
    public void run() {

        try {
            Thread.sleep(10*60000); // add a delay to be able to add data for prediction (10 minutes)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        predictionDatetTime = previousPredictionDateTime.plusHours(intervalTimeHours);
        LOGGER.info("Started asynchronous dayahead prediction for date " + predictionDatetTime);

        previousPredictionDateTime = currentDateTime;

        String dataCenterId = deviceRepository.getDeviceIdByLabel(dataCenterName);

        //if(debugMode) {
        LOGGER.info("Started dayahead prediction for datacenter " + dataCenterName + " with prediction type " + predictionType
                    + " with flexibility type " + flexibilityType + " with granularity " + granularity + " for date " + currentDateTime.plusHours(intervalTimeHours));
        //}

        // check all flags and execute predictions appropiately
        if(dayaheadDCEnergyConsumption == true){
            // check if server prediction is on
            if(dayaheadServerEnergyConsumption == true){
                // perform server prediction
                // find componentID based on scenario and componentType
                String serverComponentId = deviceRepository.getDeviceIdByLabelCompoenent("SERVER-ROOM");
                if(debugMode) {
                    LOGGER.info("Performing SERVER_ROOM prediction...");
                }
                computePredictionAndSaveIntoDB(scenario, predictionType, granularity, dataCenterId,"SERVER_ROOM", serverComponentId, flexibilityType);
                if(debugMode) {
                    LOGGER.info("Finished SERVER_ROOM prediction...");
                }
            }

            if(dayaheadCoolingEnergyConsumption == true){
                // perform cooling prediction
                // find componentID based on scenario and componentType
                String coolingComponentId = deviceRepository.getDeviceIdByLabelCompoenent("COOLING-SYSTEM");
                if(debugMode) {
                    LOGGER.info("Performing COOLING_SYSTEM prediction...");
                }
                computePredictionAndSaveIntoDB(scenario, predictionType, granularity, dataCenterId,"COOLING_SYSTEM", coolingComponentId, flexibilityType);
                if(debugMode) {
                    LOGGER.info("Finished COOLING_SYSTEM prediction...");
                }
            }
        }

        LOGGER.info("Finished asynchronous dayahead prediction for date " + currentDateTime.plusHours(intervalTimeHours));

        currentDateTime = currentDateTime.plusHours(intervalTimeHours);

    }


    private void computePredictionAndSaveIntoDB(String scenario,
                                           String predictionType,
                                           String granularity,
                                           String dataCenterID,
                                           String componentType,
                                           String componentID,
                                           String flexibilityType){
        //train and save model
        EnergyProfileDTO predictedEnergyProfile = algorithmsBroker.computeEnergyPredictionRealTime(DataScenario.getDataScenario(scenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                DeviceTypeEnum.setType(componentType),
                UUID.fromString(componentID),
                DateUtils.millisToLocalDateTime(currentDateTime.plusHours(intervalTimeHours).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()),
                FlexibilityType.setType(flexibilityType));


        // insert in database only if there are predictions
        if(!predictedEnergyProfile.getCurve().equals(EnergyProfileDTO.getDefaultInstance(startTimeForPrediction, PredictionGranularity.valueOf(granularity.toUpperCase())).getCurve())){

            // if prediction is for SERVER_ROOM compute the DT predictions as percentages from RT predictions
            // then split values into two different curves for RT and DT
            if(componentType.equals(DeviceTypeEnum.SERVER_ROOM.type())){

                // the server RT prediction is the actual prediction
                EnergyProfileDTO predictedEnergyProfileRT = predictedEnergyProfile;

                // the server DT prediction is computed as percentage from RT
                long startTime = currentDateTime.plusHours(intervalTimeHours).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                EnergyProfileDTO predictedEnergyProfileDT = predictionServiceUtils.computePredictedDTCurveFromRTCurve(predictedEnergyProfileRT, dataCenterName, startTime);

                // insert energy profiles into database
                if(debugMode) {
                    LOGGER.info("The predicted energy curves are being stored in DB");
                }
                // insert predicted values & prediction job
                PredictionJobDTO predictionJobDTO = new PredictionJobDTO(UUID.randomUUID(),
                        LocalDateTime.now(),
                        granularity.toUpperCase(),
                        AlgorithmType.ENSEMBLE.getAlgorithmName().toUpperCase(),
                        flexibilityType);
                predictionServiceUtils.truncateNegativePredictedValues(predictedEnergyProfileDT);
                energyProfileService.postEnergyProfilePredicted(predictedEnergyProfileDT, predictionJobDTO);
                predictionServiceUtils.truncateNegativePredictedValues(predictedEnergyProfileRT);
                energyProfileService.postEnergyProfilePredicted(predictedEnergyProfileRT, predictionJobDTO);

            }else{
                // insert energy profile into database
                if(debugMode) {
                    LOGGER.info("The predicted energy curve is being stored in DB");
                }
                // insert predicted values & prediction job
                PredictionJobDTO predictionJobDTO = new PredictionJobDTO(UUID.randomUUID(),
                        LocalDateTime.now(),
                        granularity.toUpperCase(),
                        AlgorithmType.ENSEMBLE.getAlgorithmName().toUpperCase(),
                        flexibilityType);
                predictionServiceUtils.truncateNegativePredictedValues(predictedEnergyProfile);
                energyProfileService.postEnergyProfilePredicted(predictedEnergyProfile, predictionJobDTO);
            }
        }
    }
}
