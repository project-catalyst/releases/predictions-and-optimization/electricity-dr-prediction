package ro.tuc.dsrl.catalyst.ebbservice.remote_connection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RestAPIConfig {

    @Value("${python.url}")
    private String pythonHostUrl;

    @Value("${dbapi.url}")
    private String dbApiHostUrl;

    public String getPythonHostUrl() {
        return pythonHostUrl;
    }

    public String getDbApiHostUrl() {
        return  dbApiHostUrl;
    }
}
