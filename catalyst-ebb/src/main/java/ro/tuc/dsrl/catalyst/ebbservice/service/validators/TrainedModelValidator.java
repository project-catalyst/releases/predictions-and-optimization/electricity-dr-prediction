package ro.tuc.dsrl.catalyst.ebbservice.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.ebbservice.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.enums.AlgorithmType;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.util.ArrayList;
import java.util.List;

public class TrainedModelValidator {

    private static final Log LOGGER = LogFactory.getLog(TrainedModelValidator.class);

    private TrainedModelValidator(){

    }

    public static void validateParams(DataScenario dataScenario,
                                      PredictionGranularity predictionGranularity,
                                      AlgorithmType algorithmType,
                                      DeviceTypeEnum componentType) {

        List<String> errors = new ArrayList<>();

        if (dataScenario.getScenario() == null || dataScenario.getScenario().equals("")) {
            errors.add("Data scenario must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(TrainedModelValidator.class.getSimpleName(), errors);
        }

        if (predictionGranularity.getGranularity() == null || predictionGranularity.getGranularity().equals("")) {
            errors.add("Prediction granularity must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(TrainedModelValidator.class.getSimpleName(), errors);
        }

        if (algorithmType.getShortName() == null || algorithmType.getShortName().equals("")) {
            errors.add("Algorithm type must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(TrainedModelValidator.class.getSimpleName(), errors);
        }

        if (componentType.type() == null || componentType.type().equals("")) {
            errors.add("Topology component must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(TrainedModelValidator.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(TrainedModelValidator.class.getSimpleName(), errors);
        }
    }
}
