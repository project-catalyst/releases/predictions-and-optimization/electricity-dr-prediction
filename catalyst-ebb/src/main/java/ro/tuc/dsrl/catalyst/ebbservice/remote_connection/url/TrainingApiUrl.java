package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

public enum TrainingApiUrl implements ApiUrl{

    POST_NEAR_REALTIME_TRAINED_MODEL("near_real_time/train/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_INTRADAY_TRAINED_MODEL("intraday/train/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_DAYAHEAD_TRAINED_MODEL("dayahead/train/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),

    POST_DAYAHEAD_UPPER_FLEXIBILITY_TRAINED_MODEL("dayahead/train_flexibility_upper_bounds/{algorithmType}/{dataScenario}/{componentType}"),
    POST_DAYAHEAD_LOWER_FLEXIBILITY_TRAINED_MODEL("dayahead/train_flexibility_lower_bounds/{algorithmType}/{dataScenario}/{componentType}");

    private String value;

    TrainingApiUrl(String url) {
        this.value = url;
    }

    public String value() {
        return value;
    }
}
