package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces;

import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.time.LocalDateTime;

public interface IAlgorithmAccess {

    EnergyProfileDTO computePrediction(EnergyProfileDTO curve,
                                       PredictionGranularity granularity,
                                       DataScenario dataScenario,
                                       LocalDateTime startTime,
                                       DeviceTypeEnum componentType,
                                       FlexibilityType flexibilityType,
                                       String trainingOn);

    String trainModelForPrediction(EnergyProfileDTO trainingDataCurve,
                                   PredictionGranularity granularity,
                                   DataScenario dataScenario,
                                   LocalDateTime endTime,
                                   DeviceTypeEnum componentType,
                                   FlexibilityType flexibilityType);

    EnergyProfileDTO loadModelAndComputePrediction(EnergyProfileDTO energyProfile,
                                                   PredictionGranularity predictionGranularity,
                                                   DataScenario dataScenario, LocalDateTime startTime,
                                                   DeviceTypeEnum componentType,
                                                   FlexibilityType flexibilityType);
}
