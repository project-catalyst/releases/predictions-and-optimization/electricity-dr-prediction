package ro.tuc.dsrl.catalyst.ebbservice.util;

import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.PredictionApiUrl;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

public enum PredictionApi {

    DAYAHEAD(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_DAYAHEAD_PREDICTION),
    INTRADAY(PredictionGranularity.INTRADAY, PredictionApiUrl.POST_INTRADAY_PREDICTION),
    NEAR_REAL_TIME(PredictionGranularity.NEAR_REAL_TIME, PredictionApiUrl.POST_NEAR_REALTIME_PREDICTION),

    //FLEXIBILITY
    FLEXIIBILITY_UPPER(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_DAYAHEAD_UPPER_FLEXIBILITY),
    FLEXIIBILITY_LOWER(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_DAYAHEAD_LOWER_FLEXIBILITY),

    DAYAHEAD_LOAD_MODEL_AND_PREDICT(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_DAYAHEAD_LOAD_MODEL_AND_PREDICT),
    INTRADAY_LOAD_MODEL_AND_PREDICT(PredictionGranularity.INTRADAY, PredictionApiUrl.POST_INTRADAY_LOAD_MODEL_AND_PREDICT),
    NEAR_REAL_TIME_LOAD_MODEL_AND_PREDICT(PredictionGranularity.NEAR_REAL_TIME, PredictionApiUrl.POST_NEAR_REAL_TIME_LOAD_MODEL_AND_PREDICT),
    FLEXIIBILITY_UPPER_LOAD_MODEL_AND_PREDICT(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_FLEXIBILITY_UPPER_LOAD_MODEL_AND_PREDICT),
    FLEXIIBILITY_LOWER_LOAD_MODEL_AND_PREDICT(PredictionGranularity.DAYAHEAD, PredictionApiUrl.POST_FLEXIBILITY_LOWER_LOAD_MODEL_AND_PREDICT);


    private PredictionGranularity predictionGranularity;
    private PredictionApiUrl url;

    PredictionApi(PredictionGranularity predictionGranularity, PredictionApiUrl predictionApiUrl) {
        this.predictionGranularity = predictionGranularity;
        this.url = predictionApiUrl;
    }

    public PredictionApiUrl getApiUrl() {
        return url;
    }

    public static PredictionApi getPredictionApiByPredictionGranularityAndFlexibilityType(
            PredictionGranularity predictionGranularity,
            FlexibilityType flexibilityType) {

       if(predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("NONE"))
           return PredictionApi.DAYAHEAD;
       else if (predictionGranularity.getGranularity().equals(Constants.INTRADAY) && flexibilityType.getType().equals("NONE"))
           return PredictionApi.INTRADAY;
       else if (predictionGranularity.getGranularity().equals(Constants.NEAR_REAL_TIME) && flexibilityType.getType().equals("NONE"))
           return PredictionApi.NEAR_REAL_TIME;
       else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("UPPER"))
           return PredictionApi.FLEXIIBILITY_UPPER;
       else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("LOWER"))
           return PredictionApi.FLEXIIBILITY_LOWER;
       else return null;
    }

    public static PredictionApi getApiByPredictionGranularityAndFlexibilityType(PredictionGranularity predictionGranularity,
                                                                              FlexibilityType flexibilityType) {

        if(predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("NONE"))
            return PredictionApi.DAYAHEAD_LOAD_MODEL_AND_PREDICT;
        else if (predictionGranularity.getGranularity().equals(Constants.INTRADAY) && flexibilityType.getType().equals("NONE"))
            return PredictionApi.INTRADAY_LOAD_MODEL_AND_PREDICT;
        else if (predictionGranularity.getGranularity().equals(Constants.NEAR_REAL_TIME) && flexibilityType.getType().equals("NONE"))
            return PredictionApi.NEAR_REAL_TIME_LOAD_MODEL_AND_PREDICT;
        else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("UPPER"))
            return PredictionApi.FLEXIIBILITY_UPPER_LOAD_MODEL_AND_PREDICT;
        else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("LOWER"))
            return PredictionApi.FLEXIIBILITY_LOWER_LOAD_MODEL_AND_PREDICT;
        else return null;
    }
}
