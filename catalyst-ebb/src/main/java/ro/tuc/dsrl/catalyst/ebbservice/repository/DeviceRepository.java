package ro.tuc.dsrl.catalyst.ebbservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ServerRoomDTO;
import ro.tuc.dsrl.geyser.datamodel.components.consumption.it.ServerRoom;

import java.time.LocalDateTime;
import java.util.*;

@Repository
public class DeviceRepository {

    private final RestAPIClient accessPoint;

    @Autowired
    public DeviceRepository(RestAPIClient accessPoint) {
        this.accessPoint = accessPoint;
    }


    public String getDeviceIdByLabel(String label) {

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("device-name", label);

        return accessPoint.getObject(
                DbApiUrl.GET_COMPONENT_ID_FOR_LABEL,
                String.class,
                urlVariables);

    }

    public String getDeviceIdByLabelCompoenent(String label){

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("device-name", label);

        return accessPoint.getObject(
                DbApiUrl.GET_COMPONENT_ID_FOR_LABEL_COMPONENT,
                String.class,
                urlVariables);
    }

    public List<ServerRoomDTO> getServerRooms(String dcName, Long startTime){

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dc-name", dcName);
        urlVariables.put("startTime", startTime);

        ServerRoomDTO[] serverRoomDTOS = accessPoint.getObjects(
                DbApiUrl.GET_SERVER_ROOMS_FOR_DC,
                ServerRoomDTO[].class,
                urlVariables);
        List<ServerRoomDTO> serverRoomDTOList = new ArrayList<>(Arrays.asList(serverRoomDTOS));

        return serverRoomDTOList;
    }

}
