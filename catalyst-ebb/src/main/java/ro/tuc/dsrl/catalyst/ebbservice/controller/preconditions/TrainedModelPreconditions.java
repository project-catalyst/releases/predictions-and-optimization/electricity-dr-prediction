package ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions;

import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class TrainedModelPreconditions {

    private TrainedModelPreconditions() {
    }

    public static void validateParams(String dataScenario, String granularity, String dataCenterID, String componentType, String componentID) {

        UUIDPreconditions.validate(dataCenterID);
        UUIDPreconditions.validate(componentID);

        List<String> errors = new ArrayList<>();

        if (dataScenario == null || dataScenario.equals("")) {
            errors.add("Data scenario must not be null");
        }

        if (granularity == null || granularity.equals("")) {
            errors.add("Granularity must not be null");
        }

        if (componentType == null || componentType.equals("")) {
            errors.add("Component type must not be null");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect parameters", errors);
        }
    }
}
