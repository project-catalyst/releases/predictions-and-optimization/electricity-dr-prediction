package ro.tuc.dsrl.catalyst.ebbservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TrainedModelRepository {

    private final RestAPIClient accessPoint;

    @Autowired
    public TrainedModelRepository(RestAPIClient accessPoint) {
        this.accessPoint = accessPoint;
    }

    public String getStatusOnLatestTrainedModel(String dataScenario,
                                                String predictionGranularity,
                                                String algorithmType,
                                                String topologyComponentType) {

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dataScenario", dataScenario);
        urlVariables.put("predictionGranularity", predictionGranularity);
        urlVariables.put("algorithmType", algorithmType);
        urlVariables.put("topologyComponentType", topologyComponentType);

       return  accessPoint.getObject(
                DbApiUrl.GET_STATUS_OF_LATEST_TRAINED_MODEL,
                String.class,
                urlVariables);
    }
}
