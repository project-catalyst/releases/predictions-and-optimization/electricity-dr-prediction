package ro.tuc.dsrl.catalyst.ebbservice.util;

public class Constants {

   private Constants(){

   }

   public static final String  DATACENTER_ID = "dataCenterID";

   public static final String START_TIME = "startTime";

   public static final String SERVER_ID = "serverID";

   // LITERALS

   public static final String DAYAHEAD = "DAYAHEAD";

   public static final String INTRADAY = "INTRADAY";

   public static final String NEAR_REAL_TIME = "NEAR_REAL_TIME";

}
