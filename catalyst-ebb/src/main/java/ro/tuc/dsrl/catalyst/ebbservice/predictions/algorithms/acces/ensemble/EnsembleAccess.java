package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.ensemble;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.EnergyProfileDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.mlp.MultiLayerPerceptronAccess;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.ebbservice.util.TrainingApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.TrainedModelDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Component("ensemble-access")
public class EnsembleAccess implements IAlgorithmAccess {

    private static final Logger LOGGER = Logger.getLogger(MultiLayerPerceptronAccess.class.getName());

    private final EnsembleRestClient algorithmRESTClient;
    private final RestAPIClient accessPoint;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public EnsembleAccess(EnsembleRestClient algorithmRESTClient, RestAPIClient accessPoint) {
        this.algorithmRESTClient = algorithmRESTClient;
        this.accessPoint = accessPoint;
    }


    @Override
    public EnergyProfileDTO computePrediction(
            EnergyProfileDTO energyProfileDTO,
            PredictionGranularity granularity,
            DataScenario dataScenario,
            LocalDateTime startTime,
            DeviceTypeEnum componentType,
            FlexibilityType flexibilityType,
            String trainingOn) {

        if(debugMode) {
            LOGGER.info("ENSEMBLE is computing prediction...");
        }

        List<Double> energyPredictedValues = algorithmRESTClient.predictEnergyValues(
                    energyProfileDTO,
                    PredictionApi.getPredictionApiByPredictionGranularityAndFlexibilityType(granularity, flexibilityType),
                    dataScenario,
                    AlgorithmType.ENSEMBLE,
                    componentType,
                    startTime.toString());

        return EnergyProfileDTOBuilder.fromPredictionOutputData(
                energyPredictedValues,
                energyProfileDTO,
                startTime);
    }

    @Override
    public String trainModelForPrediction(EnergyProfileDTO trainingDataCurve,
                                          PredictionGranularity granularity,
                                          DataScenario dataScenario,
                                          LocalDateTime endTime,
                                          DeviceTypeEnum componentType,
                                          FlexibilityType flexibilityType) {

        if(debugMode) {
            LOGGER.info("ENSEMBLE is constructing training model...");
        }

        // insert into database a new trained model entry
        TrainedModelDTO trainedModelDTO = new TrainedModelDTO(dataScenario.getScenario(),
                granularity.getGranularity(),
                AlgorithmType.ENSEMBLE.getAlgorithmName(),
                componentType.type(),
                "NONE",
                "PENDING",
                LocalDateTime.now());

        UUID trainedModelID = accessPoint.postObject(DbApiUrl.POST_TRAINED_MODEL,
                trainedModelDTO,
                UUID.class);

        String trainedModelPath = algorithmRESTClient.trainModel(
                trainingDataCurve,
                TrainingApi.getTrainingApiByPredictionGranularityAndFlexibilityType(granularity, flexibilityType),
                dataScenario,
                AlgorithmType.ENSEMBLE,
                componentType,
                endTime.toString());

        trainedModelDTO.setPathOnDisk(trainedModelPath);
        trainedModelDTO.setId(trainedModelID);

        // once training is completed update status
        accessPoint.postObject(DbApiUrl.UPDATE_TRAINED_MODEL_STATUS,
                trainedModelDTO,
                UUID.class);

        return "Completed training";
    }

    @Override
    public EnergyProfileDTO loadModelAndComputePrediction(EnergyProfileDTO energyProfile,
                                                          PredictionGranularity predictionGranularity,
                                                          DataScenario dataScenario,
                                                          LocalDateTime startTime,
                                                          DeviceTypeEnum componentType,
                                                          FlexibilityType flexibilityType) {

        if(debugMode) {
            LOGGER.info("ENSEMBLE is loading model and computing prediction...");
        }

        List<Double> energyPredictedValues = algorithmRESTClient.predictEnergyValues(
                energyProfile,
                PredictionApi.getApiByPredictionGranularityAndFlexibilityType(predictionGranularity, flexibilityType),
                dataScenario,
                AlgorithmType.ENSEMBLE,
                componentType,
                startTime.toString());

        EnergyProfileDTO predictedEnergyProfileDTO =  EnergyProfileDTOBuilder.fromPredictionOutputData(
                energyPredictedValues,
                energyProfile,
                startTime);

        return predictedEnergyProfileDTO;
    }

}
