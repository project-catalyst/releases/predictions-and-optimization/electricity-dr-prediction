package ro.tuc.dsrl.catalyst.ebbservice.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.ebbservice.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;

import java.util.ArrayList;
import java.util.List;

public class PredictionJobValidator {

    private static final Log LOGGER = LogFactory.getLog(PredictionJobValidator.class);

    private PredictionJobValidator(){

    }

    public static void validatePredictionJob(PredictionJobDTO predictionJobDTO) {

        List<String> errors = new ArrayList<>();

        if (predictionJobDTO == null) {
            errors.add("PredictionJobDTO must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }

        if (predictionJobDTO.getId() == null) {
            errors.add("PredictionJob id must not be null.");
        }

        if (predictionJobDTO.getStartTime() == null) {
            errors.add("PredictionJobDTO startTime must not be null.");
        }

        if (predictionJobDTO.getGranularity() == null || predictionJobDTO.getGranularity().isEmpty()) {
            errors.add("PredictionJob granularity is null or empty.");
        }

        if (predictionJobDTO.getAlgoType() == null || predictionJobDTO.getAlgoType().isEmpty()) {
            errors.add("PredictionJob algoType is null or empty.");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }
    }
}
