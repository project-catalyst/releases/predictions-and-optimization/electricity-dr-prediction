package ro.tuc.dsrl.catalyst.ebbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions.TrainedModelPreconditions;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.AlgorithmsBroker;
import ro.tuc.dsrl.catalyst.ebbservice.service.TrainedModelService;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.util.UUID;

@RestController
@RequestMapping("/electrical/prediction")
public class EnergyPredictionController {

    private final AlgorithmsBroker algorithmsBroker;
    private final TrainedModelService trainedModelService;

    @Autowired
    public EnergyPredictionController(AlgorithmsBroker algorithmsBroker, TrainedModelService trainedModelService) {
        this.algorithmsBroker = algorithmsBroker;
        this.trainedModelService = trainedModelService;
    }

    @GetMapping(value = "/hello")
    public String sayHello(){
        return "Hello";
    }

    @GetMapping(value = "/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO getEnergyPredictionForEntireDC(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validateDatacenter(dataScenario, predictionType, flexibilityType, granularity,
                dataCenterID, startTime);

        return algorithmsBroker.computeEnergyPrediction(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                DeviceTypeEnum.DATACENTER,
                UUID.fromString(dataCenterID),
                DateUtils.millisToLocalDateTime(startTime),
                FlexibilityType.setType(flexibilityType));
    }

    /* TODO: we can write only a single method
        we do not use componentID when retrieving data
        - use only a single method!
        - delete endTime and componentID*/
    @GetMapping(value = "/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/{component-type}/{component-id}/{startTime}/{endTime}")
    public EnergyProfileDTO getEnergyPredictionForComponent(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("component-type") String componentType,
            @PathVariable("component-id") String componentID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validateComponent(dataScenario, granularity, dataCenterID, componentType,
                componentID, startTime);

        return algorithmsBroker.computeEnergyPrediction(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                DeviceTypeEnum.setType(componentType),
                UUID.fromString(componentID),
                DateUtils.millisToLocalDateTime(startTime),
                FlexibilityType.setType(flexibilityType));
    }


    @GetMapping(value = "/train/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/{component-type}/{component-id}/{endTime}")
    public String trainModelForComponent(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("component-type") String componentType,
            @PathVariable("component-id") String componentID,
            @PathVariable("endTime") Long endTime) {

        EnergyProfilePreconditions.validateComponent(dataScenario, granularity, dataCenterID, componentType,
                componentID, endTime);

        // call method from broker to train
        return algorithmsBroker.trainModelForEnergyPrediction(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                DeviceTypeEnum.setType(componentType),
                UUID.fromString(componentID),
                DateUtils.millisToLocalDateTime(endTime),
                FlexibilityType.setType(flexibilityType));
    }

    @GetMapping(value = "/predict/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/{component-type}/{component-id}/{startTime}")
    public EnergyProfileDTO predictEnergyForComponent(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("component-type") String componentType,
            @PathVariable("component-id") String componentID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validateComponent(dataScenario, granularity, dataCenterID, componentType,
                componentID, startTime);

        // call method from broker to predict
        return algorithmsBroker.computeEnergyPredictionRealTime(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                DeviceTypeEnum.setType(componentType),
                UUID.fromString(componentID),
                DateUtils.millisToLocalDateTime(startTime),
                FlexibilityType.setType(flexibilityType));
    }

    @GetMapping(value = "/latest_trained_model_status/{data-scenario}/{prediction-type}/{granularity}/{dataCenterID}/{component-type}/{component-id}")
    public String getStatusOfLatestTrainedModelForComponent(@PathVariable("data-scenario") String dataScenario,
                                                            @PathVariable("prediction-type") String predictionType,
                                                            @PathVariable("granularity") String granularity,
                                                            @PathVariable("dataCenterID") String dataCenterID,
                                                            @PathVariable("component-type") String componentType,
                                                            @PathVariable("component-id") String componentID) {

        TrainedModelPreconditions.validateParams(dataScenario, granularity, dataCenterID, componentType,
                componentID);

        return trainedModelService.getStatusOnLatestTrainedModel(DataScenario.getDataScenario(dataScenario),
                PredictionGranularity.setGranularity(granularity.toUpperCase()),
                AlgorithmType.ENSEMBLE,
                DeviceTypeEnum.setType(componentType));

    }
}
