package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;

public enum DbApiUrl implements ApiUrl {

    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE("electrical/history/monitored-values/production/24hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE("electrical/history/monitored-values/production/4hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE("electrical/history/monitored-values/production/1hourbefore/{dataCenterID}/entireDC/{startTime}"),

    //GETTERS FOR PRODUCTION ONLINE TRAINING
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_24HOURS_BEFORE("electrical/history/monitored-values/production/historyBeforeDateEvery24Hours/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_4HOURS_BEFORE("electrical/history/monitored-values/production/historyBeforeDateEvery4Hours/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_1HOUR_BEFORE("electrical/history/monitored-values/production/historyBeforeDateEvery1Hour/{dataCenterID}/entireDC/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/entireDC/{startTime}"),

    //GETTERS FOR CONSUMPTION ONLINE TRAINING
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_24HOURS_BEFORE("electrical/history/monitored-values/consumption/historyBeforeDateEvery24Hours/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_4HOURS_BEFORE("electrical/history/monitored-values/consumption/historyBeforeDateEvery4Hours/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_1HOUR_BEFORE("electrical/history/monitored-values/consumption/historyBeforeDateEvery1Hour/{dataCenterID}/entireDC/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/itComponent/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/coolingSystem/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_4HOURS_BEFORE("electrical/history/monitored-values/production/4hoursbefore/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_1HOUR_BEFORE("electrical/history/monitored-values/production/1hourbefore/{dataCenterID}/server/{serverID}/{startTime}"),

    //GETTERS FOR COMPONENTS FOR ONLINE TRAINING
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_24HOURS_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery24Hours/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_4HOURS_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery4Hours/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_1HOUR_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery1Hour/{dataCenterID}/itComponent/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_24HOURS_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery24Hours/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_4HOURS_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery4Hours/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_1HOUR_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery1Hour/{dataCenterID}/coolingSystem/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_24HOURS_BEFORE_DATE("electrical/history/monitored-values/consumption/historyBeforeDateEvery24Hours/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_4HOURS_BEFORE_DATE("electrical/history/monitored-values/production/historyBeforeDateEvery4Hours/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_1HOUR_BEFORE_DATE("electrical/history/monitored-values/production/historyBeforeDateEvery1Hour/{dataCenterID}/server/{serverID}/{startTime}"),

    POST_PREDICTED_ENERGY_CURVE("predicted-value/insert_predicted_energy_curve/{predictionJobID}"),
    POST_PREDICTION_JOB("prediction-job/insert"),
    POST_TRAINED_MODEL("trained_model/insert"),
    UPDATE_TRAINED_MODEL_STATUS("trained_model/update_status"),

    GET_STATUS_OF_LATEST_TRAINED_MODEL("trained_model/status/{dataScenario}/{predictionGranularity}/{algorithmType}/{topologyComponentType}"),

    GET_COMPONENT_ID_FOR_LABEL("device/label/{device-name}"),
    GET_COMPONENT_ID_FOR_LABEL_COMPONENT("device/labelComponent/{device-name}"),
    GET_SERVER_ROOMS_FOR_DC("device/findServerRoomsForDC/{dc-name}/{startTime}"),
    GET_MEASUREMENT_ID_FOR_PROPERTY("measurement/byProperty/{property}");

    private String value;

    DbApiUrl(String url) {
        this.value = url;
    }


    @Override
    public String value() {
        return value;
    }

    public static DbApiUrl getValues(PredictionType predictionType, PredictionGranularity predictionGranularity) {

        switch (predictionType) {
            case ENERGY_PRODUCTION:

                return getProductionEndpointByPredictionGranularityForTraining(predictionGranularity);

            case ENERGY_CONSUMPTION:

                return getConsumptionEndpointByPredictionGranularityForTraining(predictionGranularity);

            default:

                return null;
        }
    }

        public static DbApiUrl getValue (PredictionType predictionType, PredictionGranularity predictionGranularity){
            switch (predictionType) {
                case ENERGY_PRODUCTION:

                    return getProductionEndpointByPredictionGranularity(predictionGranularity);

                case ENERGY_CONSUMPTION:

                    return getConsumptionEndpointByPredictionGranularity(predictionGranularity);

                default:

                    return null;
            }
        }

    public static DbApiUrl getValue(PredictionGranularity predictionGranularity, DeviceTypeEnum component){

        switch (predictionGranularity) {

            case DAYAHEAD:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_24HOURS_BEFORE;
                    case SERVER_ROOM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_24HOURS_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_24HOURS_BEFORE;
                    default:
                        return null;
                }
            case INTRADAY:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_4HOURS_BEFORE;
                    case SERVER_ROOM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_4HOURS_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_4HOURS_BEFORE;
                    default:
                        return null;
                }
            case NEAR_REAL_TIME:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_1HOUR_BEFORE;
                    case SERVER_ROOM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_1HOUR_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_1HOUR_BEFORE;
                    default:
                        return null;
                }
            default:
                return null;
        }
    }
        public static DbApiUrl getValues(PredictionGranularity predictionGranularity, DeviceTypeEnum component){

            switch (predictionGranularity) {

                case DAYAHEAD:
                    switch (component) {

                        case COOLING_SYSTEM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_24HOURS_BEFORE_DATE;
                        case SERVER_ROOM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_24HOURS_BEFORE_DATE;
                        case SERVER:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_24HOURS_BEFORE_DATE;
                        default:
                            return null;
                    }
                case INTRADAY:
                    switch (component) {

                        case COOLING_SYSTEM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_4HOURS_BEFORE_DATE;
                        case SERVER_ROOM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_4HOURS_BEFORE_DATE;
                        case SERVER:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_4HOURS_BEFORE_DATE;
                        default:
                            return null;
                    }
                case NEAR_REAL_TIME:
                    switch (component) {

                        case COOLING_SYSTEM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_EVERY_1HOUR_BEFORE_DATE;
                        case SERVER_ROOM:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_EVERY_1HOUR_BEFORE_DATE;
                        case SERVER:
                            return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_EVERY_1HOUR_BEFORE_DATE;
                        default:
                            return null;
                    }
                default:
                    return null;
            }
        }


        private static DbApiUrl getProductionEndpointByPredictionGranularity (PredictionGranularity
        predictionGranularity){

            switch (predictionGranularity) {
                case DAYAHEAD:
                    return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE;
                case INTRADAY:
                    return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE;
                case NEAR_REAL_TIME:
                    return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE;
                default:
                    return null;
            }
        }

        private static DbApiUrl getConsumptionEndpointByPredictionGranularity (PredictionGranularity
        predictionGranularity){

            switch (predictionGranularity) {
                case DAYAHEAD:
                    return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE;
                case INTRADAY:
                    return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE;
                case NEAR_REAL_TIME:
                    return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE;
                default:
                    return null;
            }
        }


    private static DbApiUrl getConsumptionEndpointByPredictionGranularityForTraining(PredictionGranularity predictionGranularity) {

        switch (predictionGranularity) {
            case DAYAHEAD:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_24HOURS_BEFORE;
            case INTRADAY:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_4HOURS_BEFORE;
            case NEAR_REAL_TIME:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_1HOUR_BEFORE;
            default:
                return null;
        }
    }

    private static DbApiUrl getProductionEndpointByPredictionGranularityForTraining(PredictionGranularity predictionGranularity) {

        switch (predictionGranularity) {
            case DAYAHEAD:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_24HOURS_BEFORE;
            case INTRADAY:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_4HOURS_BEFORE;
            case NEAR_REAL_TIME:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_FOR_TRAINING_EVERY_1HOUR_BEFORE;
            default:
                return null;
        }
    }
}
