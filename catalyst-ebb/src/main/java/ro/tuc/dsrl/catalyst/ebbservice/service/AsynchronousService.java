package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
@EnableAsync
public class AsynchronousService {

    private static final int RUNNING_PERIODICITY_DAYAHEAD = 24;
    private static final int RUNNING_PERIODICITY_INTRADAY = 4;

    private static final long INITIAL_DELAY_FOR_FAST_FWD = 1;  // 1 min initial delay

    private static final Log LOGGER = LogFactory.getLog(AsynchronousService.class);

    private final ApplicationContext applicationContext;
    @Autowired
    private final ScheduledThreadPoolExecutor  threadPoolExecutor;

    @Value("${fast.foward}")
    private boolean fastFoward;

    @Autowired
    public AsynchronousService(ApplicationContext applicationContext,
                               ScheduledThreadPoolExecutor threadPoolExecutor){
        this.applicationContext = applicationContext;
        this.threadPoolExecutor = threadPoolExecutor;
    }

    public void executeAsynchronously() throws InterruptedException {

        TrainModelAndPredictForNextDay trainModelAndPredictForNextDay = applicationContext.getBean(TrainModelAndPredictForNextDay.class);
        TrainModelAndPredictForNextPartOfDay trainModelAndPredictForNextPartOfDay = applicationContext.getBean(TrainModelAndPredictForNextPartOfDay.class);
        ScheduledFuture<?> dayaheadPredictionJob;
        ScheduledFuture<?> intradayPredictionJob;

        if(fastFoward){

            // Run in simulation mode (hours become minutes)

            // dayahead executes each 24 hours
            dayaheadPredictionJob = threadPoolExecutor.scheduleAtFixedRate(trainModelAndPredictForNextDay,
                     INITIAL_DELAY_FOR_FAST_FWD,
                     RUNNING_PERIODICITY_DAYAHEAD,
                     TimeUnit.SECONDS);

            // intraday executes each 4 hours
            intradayPredictionJob = threadPoolExecutor.scheduleAtFixedRate(trainModelAndPredictForNextPartOfDay,
                    INITIAL_DELAY_FOR_FAST_FWD,
                    RUNNING_PERIODICITY_INTRADAY,
                    TimeUnit.SECONDS);

        }
        else{

            // Run in real time mode

            //dayahead executes each 24 hours
            dayaheadPredictionJob = threadPoolExecutor.scheduleAtFixedRate(trainModelAndPredictForNextDay,
                    0,
                    RUNNING_PERIODICITY_DAYAHEAD,
                    TimeUnit.HOURS);

            // intraday executes each 4 hours
            intradayPredictionJob = threadPoolExecutor.scheduleAtFixedRate(trainModelAndPredictForNextPartOfDay,
                    0,
                    RUNNING_PERIODICITY_INTRADAY,
                    TimeUnit.HOURS);
        }


        try {
            dayaheadPredictionJob.get();
            intradayPredictionJob.get();
        } catch (Exception ex) {
            LOGGER.error(ex);
        }
    }


}