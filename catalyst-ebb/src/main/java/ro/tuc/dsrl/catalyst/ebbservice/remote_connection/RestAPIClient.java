package ro.tuc.dsrl.catalyst.ebbservice.remote_connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.PredictionApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.TrainingApiUrl;

import java.util.Map;

@Component
public final class RestAPIClient {

    private static final Log LOGGER = LogFactory.getLog(RestAPIClient.class);

    private final RestAPIConfig restAPIConfig;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public RestAPIClient(RestAPIConfig restAPIConfig) {
        this.restAPIConfig = restAPIConfig;
    }

    // DB methods
    public <T> T getObject(DbApiUrl url, Class<T> responseType, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.getObject(restAPIConfig.getDbApiHostUrl() + url.value(), responseType, urlVariables);
    }

    public <T> T[] getObjects(DbApiUrl url, Class<T[]> responseType, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.getObjects(restAPIConfig.getDbApiHostUrl() + url.value(), responseType, urlVariables);
    }

    public <T> T postObject(DbApiUrl url, Object o, Class<T> clazz) {
        return RestTemplateWrapper.postObject(restAPIConfig.getDbApiHostUrl() + url.value(), o, clazz);
    }

    public <T> T postObject(
            DbApiUrl url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.postObject(
                restAPIConfig.getDbApiHostUrl() + url.value(), o, clazz, urlVariables);
    }

    public void putObject(DbApiUrl url, Object o, Map<String, ?> urlVariables) {
        RestTemplateWrapper.putObject(restAPIConfig.getDbApiHostUrl() + url.value(), o, urlVariables);
    }


    // Prediction methods
    public <T> T postObject(PredictionApiUrl url, Object o, ParameterizedTypeReference<T> reference) {
        if(debugMode) {
            LOGGER.info("Calling... " + restAPIConfig.getPythonHostUrl() + url.value());
        }
        return RestTemplateWrapper.postObject(restAPIConfig.getPythonHostUrl() + url.value(), o, reference);
    }

    public <T> T postObject(
            PredictionApiUrl url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {
        if(debugMode) {
            LOGGER.info("Calling... " + restAPIConfig.getPythonHostUrl() + url.value());
        }
        return RestTemplateWrapper.postObject(
                restAPIConfig.getPythonHostUrl() + url.value(), o, reference, urlVariables);
    }

    // Training methods
    public <T> T postObject(
            TrainingApiUrl url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {
        LOGGER.info("Calling... " + restAPIConfig.getPythonHostUrl() + url.value());
        return RestTemplateWrapper.postObject(
                restAPIConfig.getPythonHostUrl() + url.value(), o, reference, urlVariables);
    }

    private static class RestTemplateWrapper {

        public static <T> T postObject(
                String url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {

            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference, urlVariables).getBody();
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz);
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz, urlVariables);
        }

        public static <T> T postObject(String url, Object o, ParameterizedTypeReference<T> reference) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference).getBody();
        }

        public static <T> T getObject(String url, Class<T> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.getForObject(url, responseType, urlVariables);
        }

        public static <T> T[] getObjects(String url, Class<T[]> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<T[]> response = rt.exchange(url, HttpMethod.GET, null, responseType, urlVariables);
            return response.getBody();
        }

        public static void putObject(String url, Object o, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            rt.put(url, o, urlVariables);
        }
    }
}
