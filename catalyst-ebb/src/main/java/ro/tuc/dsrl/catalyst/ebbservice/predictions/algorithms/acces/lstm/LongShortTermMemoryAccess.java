package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.lstm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.EnergyProfileDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

@Component("lstm-access")
public class LongShortTermMemoryAccess implements IAlgorithmAccess {

    private static final Logger LOGGER = Logger.getLogger(LongShortTermMemoryRESTClient.class.getName());

    private final LongShortTermMemoryRESTClient algorithmRESTClient;

    @Value("${debug.mode}")
    private boolean debugMode;

    public LongShortTermMemoryAccess(LongShortTermMemoryRESTClient algorithmRESTClient) {
        this.algorithmRESTClient = algorithmRESTClient;
    }

    @Override
    public EnergyProfileDTO computePrediction(
            EnergyProfileDTO energyProfileDTO,
            PredictionGranularity granularity,
            DataScenario dataScenario,
            LocalDateTime startTime,
            DeviceTypeEnum componentType,
            FlexibilityType flexibilityType,
            String trainingOn) {

        if(debugMode) {
            LOGGER.info("LSTM is computing prediction...");
        }

        List<Double> energyPredictedValues = algorithmRESTClient.predictEnergyValues(
                energyProfileDTO,
                PredictionApi.getPredictionApiByPredictionGranularityAndFlexibilityType(granularity, flexibilityType),
                dataScenario,
                AlgorithmType.LSTM,
                componentType,
                startTime.toString());

        return EnergyProfileDTOBuilder.fromPredictionOutputData(
                energyPredictedValues,
                energyProfileDTO,
                startTime);
    }

    @Override
    public String trainModelForPrediction(EnergyProfileDTO trainingDataCurve,
                                          PredictionGranularity granularity,
                                          DataScenario dataScenario,
                                          LocalDateTime endTime,
                                          DeviceTypeEnum componentType,
                                          FlexibilityType flexibilityType) {
        //TODO
        return null;
    }

    @Override
    public EnergyProfileDTO loadModelAndComputePrediction(EnergyProfileDTO energyProfile,
                                                          PredictionGranularity predictionGranularity,
                                                          DataScenario dataScenario,
                                                          LocalDateTime startTime,
                                                          DeviceTypeEnum componentType,
                                                          FlexibilityType flexibilityType) {
        return null;
    }

}
