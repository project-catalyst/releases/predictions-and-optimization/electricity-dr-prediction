package ro.tuc.dsrl.catalyst.ebbservice.util;

import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EnergyProfilePadding {

    private static List<Double> padWithZerosIfNeeded(EnergyProfileDTO energyProfileDTO, int size) {

        if (energyProfileDTO == null) {
            return new ArrayList<>(Collections.nCopies(size, 0.0));
        }

        List<Double> energyValues = energyProfileDTO.getEnergyValues();
        if (energyValues.size() == size) {
            return energyValues;
        }

        energyValues.addAll(Collections.nCopies(size - energyValues.size(), 0.0));
        return energyValues;
    }
}
