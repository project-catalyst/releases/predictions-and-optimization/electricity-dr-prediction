package ro.tuc.dsrl.catalyst.ebbservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class MeasurementRepository {

    private final RestAPIClient accessPoint;

    @Autowired
    public MeasurementRepository(RestAPIClient accessPoint) {
        this.accessPoint = accessPoint;
    }


    public UUID getMeasurementIdForProperty(String property) {

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("property", property);

        return accessPoint.getObject(
                DbApiUrl.GET_MEASUREMENT_ID_FOR_PROPERTY,
                UUID.class,
                urlVariables);

    }
}
