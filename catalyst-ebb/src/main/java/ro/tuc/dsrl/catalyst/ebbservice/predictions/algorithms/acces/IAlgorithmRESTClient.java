package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces;

import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

public interface IAlgorithmRESTClient<T, O> {

    O predictEnergyValues(T t, PredictionApi predictionApi, DataScenario dataScenario, AlgorithmType algorithmType,
                          DeviceTypeEnum componentType, String startTime);
}
