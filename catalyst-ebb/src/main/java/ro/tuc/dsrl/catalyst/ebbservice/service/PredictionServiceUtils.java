package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.ebbservice.repository.DeviceRepository;
import ro.tuc.dsrl.catalyst.ebbservice.repository.MeasurementRepository;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ServerRoomDTO;
import ro.tuc.dsrl.catalyst.model.enums.MeasurementType;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PredictionServiceUtils {

    private final MeasurementRepository measurementRepository;
    private final DeviceRepository deviceRepository;

    @Autowired
    public PredictionServiceUtils(MeasurementRepository measurementRepository, DeviceRepository deviceRepository) {
        this.measurementRepository = measurementRepository;
        this.deviceRepository = deviceRepository;
    }

    public EnergyProfileDTO computePredictedDTCurveFromRTCurve(EnergyProfileDTO predictedEnergyProfileRT, String dataCenterName, long startTime){

        //find first ratio for DT signal
        double dt_ratio = 0.0;
        double rt_ratio = 0.0;

        List<ServerRoomDTO> serverRooms = deviceRepository.getServerRooms(dataCenterName, startTime);
        for (ServerRoomDTO serverRoom : serverRooms){
            dt_ratio = serverRoom.getEdPercentage();
            rt_ratio = 1.0 - dt_ratio;
        }

        // the server DT prediction is a percentage of the actual prediction
        EnergyProfileDTO predictedEnergyProfileDT = new EnergyProfileDTO();
        List<EnergySampleDTO> energySampleDTOList= new ArrayList<>();
        for(int i=0;i<predictedEnergyProfileRT.getPredictionGranularity().getNoOutputs(); i++){
            energySampleDTOList.add(new EnergySampleDTO(( dt_ratio/rt_ratio) * predictedEnergyProfileRT.getCurve().get(i).getEnergyValue(),
                    predictedEnergyProfileRT.getCurve().get(i).getTimestamp()));
        }
        predictedEnergyProfileDT.setCurve(energySampleDTOList);
        predictedEnergyProfileDT.setDeviceId(predictedEnergyProfileRT.getDeviceId());
        UUID measurementId = measurementRepository.getMeasurementIdForProperty(MeasurementType.SERVER_ROOM_CURRENT_DT_CONSUMPTION.type());
        predictedEnergyProfileDT.setMeasurementId(measurementId);
        predictedEnergyProfileDT.setAggregationGranularity(predictedEnergyProfileRT.getAggregationGranularity());
        predictedEnergyProfileDT.setPredictionGranularity(predictedEnergyProfileRT.getPredictionGranularity());
        predictedEnergyProfileDT.setPredictionType(predictedEnergyProfileRT.getPredictionType());
        predictedEnergyProfileDT.setEnergyType(predictedEnergyProfileRT.getEnergyType());

        return predictedEnergyProfileDT;
    }

    public void truncateNegativePredictedValues(EnergyProfileDTO predictedEnergyProfile){

        for(EnergySampleDTO e: predictedEnergyProfile.getCurve()){
            if(e.getEnergyValue() < 0.0){
                e.setEnergyValue(0.0);
            }
        }
    }
}
