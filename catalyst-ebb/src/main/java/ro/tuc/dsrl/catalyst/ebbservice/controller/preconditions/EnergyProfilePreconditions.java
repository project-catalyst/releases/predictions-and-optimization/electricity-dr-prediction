package ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions;

import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.error_handler.ErrorMessageConstants;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class EnergyProfilePreconditions {

    private EnergyProfilePreconditions() {
    }

    public static void validateDatacenter(String dataScenario, String predictionType, String flexibilityType,
                                          String granularity, String dataCenterID, Long startTime) {

        List<String> errors = new ArrayList<>();

        String dataScenarioError = validateDataScenario(dataScenario);
        if (dataScenarioError != null) {
            errors.add(dataScenarioError);
        }

        String predictionTypeError = validatePredictionType(predictionType);
        if (predictionTypeError != null) {
            errors.add(predictionTypeError);
        }

        String flexibilityTypeError = validateFlexibilityType(flexibilityType);
        if (flexibilityTypeError != null) {
            errors.add(flexibilityTypeError);
        }

        if (granularity == null || granularity.equals("")) {
            errors.add(ErrorMessageConstants.GRANULARITY_NOT_NULL);
        }

        String dataCenterIDValidationError = UUIDPreconditions.validate(dataCenterID);
        if (dataCenterID == null) {
            errors.add(dataCenterIDValidationError);
        }

        if (startTime == null) {
            errors.add("Start time must not be null");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect parameters", errors);
        }
    }

    public static void validateComponent(String dataScenario, String granularity, String dataCenterID,
                                         String componentType, String componentID, Long startTime) {

        List<String> errors = new ArrayList<>();

        String dataScenarioError = validateDataScenario(dataScenario);
        if (dataScenarioError != null) {
            errors.add(dataScenarioError);
        }

        if (granularity == null || granularity.equals("")) {
            errors.add("Granularity must not be null");
        }

        String dataCenterIDValidationError = UUIDPreconditions.validate(dataCenterID);
        if (dataCenterIDValidationError != null) {
            errors.add(dataCenterIDValidationError);
        }

        if (componentType == null || componentType.equals("")) {
            errors.add("Granularity must not be null");
        }

        String componentIDValidationError = UUIDPreconditions.validate(componentID);
        if (componentIDValidationError != null) {
            errors.add(componentIDValidationError);
        }

        if (startTime == null) {
            errors.add("Start time must not be null");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect parameters", errors);
        }
    }

    private static String validateDataScenario(String dataScenario) {
        if (dataScenario == null || dataScenario.equals("")) {
            return "Data scenario must not be null.";
        } else if (DataScenario.getDataScenario(dataScenario) == null) {
            return "Data scenario must be PAPER or POZNAN.";
        }

        return null;
    }

    private static String validatePredictionType(String predictionType) {
        if (predictionType == null || predictionType.equals("")) {
            return "Prediction type must not be null.";
        } else if (!predictionType.equals(PredictionType.ENERGY_CONSUMPTION.getType()) &&
                !predictionType.equals(PredictionType.ENERGY_PRODUCTION.getType())) {
            return "Prediction must be PRODUCTION or CONSUMPTION";
        }

        return null;
    }

    private static String validateFlexibilityType(String flexibilityType) {
        if (flexibilityType == null || flexibilityType.equals("")) {
            return "Flexibility type must not be null";
        } else if (!flexibilityType.equals(FlexibilityType.NONE.getType()) &&
                !flexibilityType.equals(FlexibilityType.UPPER.getType()) &&
                !flexibilityType.equals(FlexibilityType.LOWER.getType())) {
            return "Flexibility must be NONE or UPPER or LOWER";
        }

        return null;
    }
}
