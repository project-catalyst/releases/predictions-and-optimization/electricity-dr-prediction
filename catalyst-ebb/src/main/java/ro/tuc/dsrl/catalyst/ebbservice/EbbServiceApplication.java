package ro.tuc.dsrl.catalyst.ebbservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.TimeZone;

//@SpringBootApplication
//public class EbbServiceApplication {
//    public static void main(String[] args) {
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//        SpringApplication.run(EbbServiceApplication.class, args);
//    }
//}


@SpringBootApplication
public class EbbServiceApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(EbbServiceApplication.class, args);
    }
}

