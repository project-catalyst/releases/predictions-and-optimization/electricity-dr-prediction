package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.mlp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmRESTClient;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class MultiLayerPerceptronRESTClient implements IAlgorithmRESTClient<EnergyProfileDTO, List<Double>> {

    private static final Logger LOGGER = Logger.getLogger(MultiLayerPerceptronRESTClient.class.getName());

    private final RestAPIClient restAccessPoint;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public MultiLayerPerceptronRESTClient(RestAPIClient restAccessPoint) {
        this.restAccessPoint = restAccessPoint;
    }

    @Override
    public List<Double> predictEnergyValues(
            EnergyProfileDTO energyProfileDTO,
            PredictionApi predictionApi,
            DataScenario dataScenario,
            AlgorithmType algorithmType,
            DeviceTypeEnum componentType,
            String startTime) {

        if(debugMode) {
            LOGGER.info("Call of MLP for predicting...");
        }

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dataScenario", dataScenario.getScenario());
        urlVariables.put("algorithmType", AlgorithmType.MLP);
        urlVariables.put("componentType", componentType.type());
        urlVariables.put("timestamp", startTime);

        return restAccessPoint.postObject(
                predictionApi.getApiUrl(),
                energyProfileDTO,
                new ParameterizedTypeReference<List<Double>>() {},
                urlVariables);
    }
}
