package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.ebbservice.repository.EnergyProfileRepository;
import ro.tuc.dsrl.catalyst.ebbservice.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.catalyst.ebbservice.service.validators.PredictionJobValidator;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EnergyProfileService {

    private final EnergyProfileRepository energyProfileRepository;

    @Autowired
    public EnergyProfileService(EnergyProfileRepository energyProfileRepository) {
        this.energyProfileRepository = energyProfileRepository;
    }

    public void postEnergyProfilePredicted(EnergyProfileDTO energyProfileDTO, PredictionJobDTO predictionJobDTO) {

        PredictionJobValidator.validatePredictionJob(predictionJobDTO);
        EnergyProfileValidator.validatePredictedEnergyProfile(energyProfileDTO);

        energyProfileRepository.insert(energyProfileDTO, predictionJobDTO);
    }

    public EnergyProfileDTO getEnergyProfile(PredictionType predictionType,
                                             PredictionGranularity predictionGranularity,
                                             UUID dataCenterID,
                                             DeviceTypeEnum componentType,
                                             UUID componentID,
                                             LocalDateTime startTime) {

        EnergyProfileValidator.validateEnergyProfileParams(predictionType, predictionGranularity,
                dataCenterID, componentType, componentID, startTime);

        EnergyProfileDTO energyProfileDTO = energyProfileRepository.getEnergyProfileMonitoredValues(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTime);

        if(energyProfileDTO == null){
            EnergyProfileDTO defaultEnergyProfileDTO = EnergyProfileDTO.getDefaultInstance(startTime, predictionGranularity);
            defaultEnergyProfileDTO.setPredictionGranularity(predictionGranularity);
            defaultEnergyProfileDTO.setPredictionType(predictionType);
        }
        else {
            // pad if needed
            List<EnergySampleDTO> paddedValues = padWithZerosIfNeeded(energyProfileDTO, predictionGranularity.getNoHours(), startTime);
            energyProfileDTO.setCurve(paddedValues);
            energyProfileDTO.setPredictionGranularity(predictionGranularity);
            energyProfileDTO.setPredictionType(predictionType);
        }

        return energyProfileDTO;
    }

    public EnergyProfileDTO getHistoryEnergyProfiles(PredictionType predictionType,
                                                           PredictionGranularity predictionGranularity,
                                                           UUID dataCenterID,
                                                           DeviceTypeEnum componentType,
                                                           UUID componentID,
                                                           LocalDateTime startTime) {

        EnergyProfileValidator.validateEnergyProfileParams(predictionType, predictionGranularity,
                dataCenterID, componentType, componentID, startTime);

        return energyProfileRepository.getHistoryEnergyProfileMonitoredValues(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTime);
    }

    private static List<EnergySampleDTO> padWithZerosIfNeeded(EnergyProfileDTO energyProfileDTO, int size, LocalDateTime startTime) {

        List<EnergySampleDTO> energyValues = new ArrayList<>();

        if (energyProfileDTO == null) {
            for (int i=0; i<size; i++){
                energyValues.add(new EnergySampleDTO(0.0, startTime.plusHours(i)));
            }
        }
        else {

            energyValues = energyProfileDTO.getCurve();
            if (energyValues.size() == size) {
                return energyValues;
            }

            for (int i = energyValues.size() + 1; i <= size; i++)
                energyValues.add(new EnergySampleDTO(0.0, startTime.plusHours(i - 1)));
            return energyValues;
        }

        return  energyValues;
    }
}
