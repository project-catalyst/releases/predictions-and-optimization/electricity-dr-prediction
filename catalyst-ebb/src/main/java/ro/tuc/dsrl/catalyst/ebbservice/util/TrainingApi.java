package ro.tuc.dsrl.catalyst.ebbservice.util;

import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.TrainingApiUrl;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

public enum TrainingApi {

    DAYAHEAD_TRAIN(PredictionGranularity.DAYAHEAD, TrainingApiUrl.POST_DAYAHEAD_TRAINED_MODEL),
    INTRADAY_TRAIN(PredictionGranularity.INTRADAY, TrainingApiUrl.POST_INTRADAY_TRAINED_MODEL),
    NEAR_REAL_TIME_TRAIN(PredictionGranularity.NEAR_REAL_TIME, TrainingApiUrl.POST_NEAR_REALTIME_TRAINED_MODEL),

    //FLEXIBILITY
    FLEXIIBILITY_UPPER(PredictionGranularity.DAYAHEAD, TrainingApiUrl.POST_DAYAHEAD_UPPER_FLEXIBILITY_TRAINED_MODEL),
    FLEXIIBILITY_LOWER(PredictionGranularity.DAYAHEAD, TrainingApiUrl.POST_DAYAHEAD_LOWER_FLEXIBILITY_TRAINED_MODEL);

    private PredictionGranularity predictionGranularity;
    private TrainingApiUrl url;

    TrainingApi(PredictionGranularity predictionGranularity, TrainingApiUrl trainingApiUrl) {
        this.predictionGranularity = predictionGranularity;
        this.url = trainingApiUrl;
    }

    public TrainingApiUrl getApiUrl() {
        return url;
    }

    public static TrainingApi getTrainingApiByPredictionGranularityAndFlexibilityType(
            PredictionGranularity predictionGranularity,
            FlexibilityType flexibilityType) {

        if(predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("NONE"))
            return TrainingApi.DAYAHEAD_TRAIN;
        else if (predictionGranularity.getGranularity().equals(Constants.INTRADAY) && flexibilityType.getType().equals("NONE"))
            return TrainingApi.INTRADAY_TRAIN;
        else if (predictionGranularity.getGranularity().equals(Constants.NEAR_REAL_TIME) && flexibilityType.getType().equals("NONE"))
            return TrainingApi.NEAR_REAL_TIME_TRAIN;
        else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("UPPER"))
            return TrainingApi.FLEXIIBILITY_UPPER;
        else if (predictionGranularity.getGranularity().equals(Constants.DAYAHEAD) && flexibilityType.getType().equals("LOWER"))
            return TrainingApi.FLEXIIBILITY_LOWER;
        else return null;
    }
}
