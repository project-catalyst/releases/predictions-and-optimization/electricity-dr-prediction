package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.ebbservice.repository.TrainedModelRepository;
import ro.tuc.dsrl.catalyst.ebbservice.service.validators.TrainedModelValidator;
import ro.tuc.dsrl.catalyst.model.enums.AlgorithmType;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

@Service
public class TrainedModelService {

    private final TrainedModelRepository trainedModelRepository;

    @Autowired
    public TrainedModelService(TrainedModelRepository trainedModelRepository) {
        this.trainedModelRepository = trainedModelRepository;
    }

    public String getStatusOnLatestTrainedModel(DataScenario dataScenario,
                                                PredictionGranularity predictionGranularity,
                                                AlgorithmType algorithmType,
                                                DeviceTypeEnum componentType) {

        TrainedModelValidator.validateParams(dataScenario, predictionGranularity, algorithmType, componentType);
        return trainedModelRepository.getStatusOnLatestTrainedModel(dataScenario.getScenario().toUpperCase(),
                predictionGranularity.getGranularity().toUpperCase(),
                algorithmType.getShortName().toUpperCase(),
                componentType.type().toUpperCase());

    }
}
