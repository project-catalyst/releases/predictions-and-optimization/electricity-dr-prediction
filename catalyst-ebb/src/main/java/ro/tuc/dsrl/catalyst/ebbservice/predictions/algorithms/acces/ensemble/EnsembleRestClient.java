package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.ensemble;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmRESTClient;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.ITrainModelRESTClient;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.ebbservice.util.TrainingApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.DeviceTypeEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class EnsembleRestClient implements IAlgorithmRESTClient<EnergyProfileDTO, List<Double>>, ITrainModelRESTClient<EnergyProfileDTO, String> {

    private static final Logger LOGGER = Logger.getLogger(EnsembleRestClient.class.getName());

    private final RestAPIClient restAccessPoint;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public EnsembleRestClient(RestAPIClient restAccessPoint) {
        this.restAccessPoint = restAccessPoint;
    }

    @Override
    public List<Double> predictEnergyValues(
            EnergyProfileDTO energyProfileDTO,
            PredictionApi predictionApi,
            DataScenario dataScenario,
            AlgorithmType algorithmType,
            DeviceTypeEnum componentType,
            String startTime) {

        if(debugMode) {
            LOGGER.info("Call of ENSEMBLE for predicting...");
        }

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dataScenario", dataScenario.getScenario());
        urlVariables.put("algorithmType", algorithmType.getAlgorithmName());
        urlVariables.put("componentType", componentType.type());
        urlVariables.put("timestamp", startTime);

        if(debugMode) {
            LOGGER.info("Calling..." + predictionApi.getApiUrl().value());
        }

        return restAccessPoint.postObject(
                predictionApi.getApiUrl(),
                energyProfileDTO,
                new ParameterizedTypeReference<List<Double>>() {},
                urlVariables);
    }

    @Override
    public String trainModel(EnergyProfileDTO trainingDataCurve,
                             TrainingApi trainingApi,
                             DataScenario dataScenario,
                             AlgorithmType algorithmType,
                             DeviceTypeEnum componentType,
                             String endTime) {

        if(debugMode) {
            LOGGER.info("Call of ENSEMBLE for training model...");
        }

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("dataScenario", dataScenario.getScenario());
        urlVariables.put("algorithmType", algorithmType.getAlgorithmName());
        urlVariables.put("componentType", componentType.type());
        urlVariables.put("timestamp", endTime);

        if(debugMode) {
            LOGGER.info("Calling..." + trainingApi.getApiUrl().value());
        }

        return restAccessPoint.postObject(
                trainingApi.getApiUrl(),
                trainingDataCurve,
                new ParameterizedTypeReference<String>() {},
                urlVariables);
    }
}
