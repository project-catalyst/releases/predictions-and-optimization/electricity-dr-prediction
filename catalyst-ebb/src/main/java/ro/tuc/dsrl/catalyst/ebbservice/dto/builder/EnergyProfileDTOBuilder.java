package ro.tuc.dsrl.catalyst.ebbservice.dto.builder;

import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EnergyProfileDTOBuilder {

    private EnergyProfileDTOBuilder() {}

    public static EnergyProfileDTO fromPredictionOutputData(
            List<Double> energyPredictedValues,
            EnergyProfileDTO energyProfileDTO,
            LocalDateTime startTime) {

        List<EnergySampleDTO> energySampleDTOs = new ArrayList<>();
        LocalDateTime sampleTime = startTime;
        PredictionGranularity granularity = energyProfileDTO.getPredictionGranularity();

        for (Double e : energyPredictedValues) {
            energySampleDTOs.add(new EnergySampleDTO(e, sampleTime));
            sampleTime = sampleTime.plusMinutes(granularity.getSampleFrequencyMin());
        }

        energyProfileDTO.setCurve(energySampleDTOs);

        return energyProfileDTO;
    }
}
