package ro.tuc.dsrl.catalyst.ebbservice.predictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmAccessFactory;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.service.EnergyProfileService;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.dbmodel.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import java.util.logging.Logger;

@Component
public class AlgorithmsBroker {

    private static final Logger LOGGER = Logger.getLogger(AlgorithmsBroker.class.getName());

    private final EnergyProfileService energyProfileService;
    private final AlgorithmAccessFactory algorithmFactory;

    @Value("${debug.mode}")
    private boolean debugMode;

    @Autowired
    public AlgorithmsBroker(EnergyProfileService energyProfileService, AlgorithmAccessFactory algorithmFactory) {
        this.energyProfileService = energyProfileService;
        this.algorithmFactory = algorithmFactory;
    }

    public EnergyProfileDTO computeEnergyPrediction(
            DataScenario dataScenario,
            PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            DeviceTypeEnum componentType,
            UUID componentID,
            LocalDateTime startTime,
            FlexibilityType flexibilityType) {

        // Set algortihmType to ensemble
        AlgorithmType algorithmType = AlgorithmType.ENSEMBLE;

        if(debugMode) {
            LOGGER.info("Requesting an energy curve from the monitored data as the input for the prediction...");
        }
        EnergyProfileDTO energyProfile = energyProfileService.getEnergyProfile(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTime.minusHours(predictionGranularity.getNoHours())
        );

        if(debugMode) {
            LOGGER.info("The Algorithm access is going to compute the prediction...");
        }
        IAlgorithmAccess algorithmAccess = algorithmFactory.getImplementation(algorithmType.name());
        EnergyProfileDTO predictedEnergyProfile = algorithmAccess.computePrediction(
                energyProfile,
                predictionGranularity,
                dataScenario,
                startTime,
                componentType,
                flexibilityType,
                "FALSE");

        if(debugMode) {
            LOGGER.info("The energy curve predicted (the output of the prediction) is being stored in DB");
        }
        // insert predicted values & prediction job
        PredictionJobDTO predictionJobDTO = new PredictionJobDTO(UUID.randomUUID(),
                LocalDateTime.now(),
                predictionGranularity.getGranularity(),
                algorithmType.name(),
                flexibilityType.getType());
        energyProfileService.postEnergyProfilePredicted(predictedEnergyProfile, predictionJobDTO);

        return predictedEnergyProfile;
    }

    public String trainModelForEnergyPrediction(
            DataScenario dataScenario,
            PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            DeviceTypeEnum componentType,
            UUID componentID,
            LocalDateTime endTime,
            FlexibilityType flexibilityType) {

        // Set algortihmType to ensemble
        AlgorithmType algorithmType = AlgorithmType.ENSEMBLE;

        // if training is on obtain also the training data up to that point
        if(debugMode) {
            LOGGER.info("Requesting the history energy curves for the training process...");
        }
        EnergyProfileDTO trainingData = energyProfileService.getHistoryEnergyProfiles(predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                endTime);

        int numberOfSamplesForTrain = trainingData.getCurve().size()/predictionGranularity.getNoOutputs();

        if(trainingData != null && !trainingData.getCurve().isEmpty() && numberOfSamplesForTrain > 2) {
            if(debugMode) {
                LOGGER.info("The Algorithm access is going to train the model for prediction...");
            }
            IAlgorithmAccess algorithmAccess = algorithmFactory.getImplementation(algorithmType.name());
            return algorithmAccess.trainModelForPrediction(
                    trainingData,
                    predictionGranularity,
                    dataScenario,
                    endTime,
                    componentType,
                    flexibilityType);
        }

        else return "Model could not be trained";
    }

    public EnergyProfileDTO computeEnergyPredictionRealTime(DataScenario dataScenario,
                                                             PredictionType predictionType,
                                                             PredictionGranularity predictionGranularity,
                                                             UUID dataCenterID,
                                                             DeviceTypeEnum componentType,
                                                             UUID componentID,
                                                             LocalDateTime startTime,
                                                             FlexibilityType flexibilityType) {

        // Set algortihmType to ensemble
        AlgorithmType algorithmType = AlgorithmType.ENSEMBLE;

        // luam dublu pentru ca nu avem date pe ziua curenta
        LocalDateTime startTimeForInputCurve =  startTime.minusHours(2*predictionGranularity.getNoHours());
        if(debugMode) {
            LOGGER.info("Start time for input curve " + startTimeForInputCurve);
            LOGGER.info("Requesting an energy curve from the monitored data as the input for the prediction...");
        }
        EnergyProfileDTO energyProfile = energyProfileService.getEnergyProfile(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTimeForInputCurve);

        if(debugMode) {
            LOGGER.info("Loaded the input curve of date: " + startTimeForInputCurve);
        }

        if(energyProfile != null && !energyProfile.getCurve().isEmpty()) {
            if(debugMode) {
                LOGGER.info("The Algorithm access is going to load the specific trained model and perform the prediction...");
            }
            IAlgorithmAccess algorithmAccess = algorithmFactory.getImplementation(algorithmType.name());
            EnergyProfileDTO predictedEnergyProfile = algorithmAccess.loadModelAndComputePrediction(
                    energyProfile,
                    predictionGranularity,
                    dataScenario,
                    startTime,
                    componentType,
                    flexibilityType);

            return predictedEnergyProfile;
        }

        return EnergyProfileDTO.getDefaultInstance(startTime,predictionGranularity);
    }

}
