from collections import namedtuple

# Parameters contract for algorithms
MLP_PARAMETERS_NAME = ['no_inputs', 'no_outputs', 'no_neurons', 'no_hidden_layers', 'batch_size', 'no_epochs']
LSTM_PARAMETERS_NAME = ['no_inputs', 'no_outputs', 'no_neurons', 'no_features', 'batch_size', 'no_epochs']


# ### dayahead
MLP_DAYAHEAD = namedtuple('MLP_DAYAHEAD', MLP_PARAMETERS_NAME)
MLP_DAYAHEAD_SERVER = [
    # model 0
    MLP_DAYAHEAD(no_inputs=25, no_outputs=24, no_neurons=24, no_hidden_layers=1, batch_size=200, no_epochs=490)
]
MLP_DAYAHEAD_COOLING = [
    # model 0
    MLP_DAYAHEAD(no_inputs=25, no_outputs=24, no_neurons=24, no_hidden_layers=1, batch_size=200, no_epochs=490)
]

MLP_DAYAHEAD_SERVER_POZNAN = [
    # model 0
    MLP_DAYAHEAD(no_inputs=24, no_outputs=24, no_neurons=35, no_hidden_layers=1, batch_size=2, no_epochs=100)
]
MLP_DAYAHEAD_COOLING_POZNAN = [
    # model 0
    MLP_DAYAHEAD(no_inputs=24, no_outputs=24, no_neurons=37, no_hidden_layers=1, batch_size=2, no_epochs=100)
]


LSTM_DAYAHEAD = namedtuple('LSTM_DAYAHEAD', LSTM_PARAMETERS_NAME)
LSTM_DAYAHEAD_SERVER = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=25, no_outputs=24, no_neurons=24, no_features=1, batch_size=16, no_epochs=600)
]
LSTM_DAYAHEAD_COOLING = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=25, no_outputs=24, no_neurons=24, no_features=1, batch_size=16, no_epochs=600)
]

LSTM_DAYAHEAD_SERVER_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=24, no_neurons=37, no_features=1, batch_size=16, no_epochs=500)
]
LSTM_DAYAHEAD_COOLING_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=24, no_neurons=40, no_features=1, batch_size=2, no_epochs=600)
]


# ### intraday
MLP_INTRADAY = namedtuple('MLP_INTRADAY', MLP_PARAMETERS_NAME)
MLP_INTRADAY_SERVER = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=24, no_hidden_layers=1, batch_size=8, no_epochs=200),
    # model 1
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 2
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=200),
    # model 3
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 4
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=12, no_hidden_layers=1, batch_size=20, no_epochs=250),
    # model 5
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=200)
]
MLP_INTRADAY_COOLING = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 1
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 2
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 3
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 4
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=170),
    # model 5
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=200)
]

MLP_INTRADAY_SERVER_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
    # model 1
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
    # model 2
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
    # model 3
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
    # model 4
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
    # model 5
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200)
]

MLP_INTRADAY_COOLING_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 1
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 2
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 3
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 4
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100),
    # model 5
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=32, no_epochs=100)
]

LSTM_INTRADAY = namedtuple('LSTM_INTRADAY', LSTM_PARAMETERS_NAME)
LSTM_INTRADAY_SERVER = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 1
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=12, no_epochs=300),
    # model 2
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 3
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 4
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 5
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300)
]
LSTM_INTRADAY_COOLING = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 1
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=270),
    # model 2
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=30, no_features=1, batch_size=16, no_epochs=175),
    # model 3
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=20, no_features=1, batch_size=80, no_epochs=400),
    # model 4
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=12, no_epochs=400),
    # model 5
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=310)
]

LSTM_INTRADAY_SERVER_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 1
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 2
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 3
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 4
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300),
    # model 5
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=300)

]
LSTM_INTRADAY_COOLING_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 1
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 2
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 3
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 4
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230),
    # model 5
    LSTM_DAYAHEAD(no_inputs=8, no_outputs=8, no_neurons=16, no_features=1, batch_size=16, no_epochs=230)
]


# ### near real time
MLP_NEAR_REAL_TIME = namedtuple('MLP_NEAR_REAL_TIME', MLP_PARAMETERS_NAME)
MLP_NEAR_REAL_TIME_SERVER = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=24, no_hidden_layers=1, batch_size=300, no_epochs=120),
]
MLP_NEAR_REAL_TIME_COOLING = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=24, no_hidden_layers=1, batch_size=300, no_epochs=120),
]

MLP_NEAR_REAL_TIME_SERVER_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=37, no_hidden_layers=1, batch_size=2, no_epochs=100),
]
MLP_NEAR_REAL_TIME_COOLING_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=37, no_hidden_layers=1, batch_size=2, no_epochs=100),
]

LSTM_NEAR_REAL_TIME = namedtuple('LSTM_NEAR_REAL_TIME', LSTM_PARAMETERS_NAME)
LSTM_NEAR_REAL_TIME_SERVER = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=1, no_neurons=17, no_features=1, batch_size=16, no_epochs=100)
]
LSTM_NEAR_REAL_TIME_COOLING = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=1, no_neurons=47, no_features=1, batch_size=32, no_epochs=600)
]

LSTM_NEAR_REAL_TIME_SERVER_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=1, no_neurons=40, no_features=1, batch_size=2, no_epochs=600)
]
LSTM_NEAR_REAL_TIME_COOLING_POZNAN = [
    # model 0
    LSTM_DAYAHEAD(no_inputs=24, no_outputs=1, no_neurons=17, no_features=1, batch_size=16, no_epochs=100)
]


# ### ### Flexibility
MLP_DAYAHEAD_FLEXIBILITY = namedtuple('MLP_DAYAHEAD_FLEXIBILITY', MLP_PARAMETERS_NAME)
MLP_DAYAHEAD_UPPER_BOUNDS = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=40, no_hidden_layers=1, batch_size=300, no_epochs=600)
]
MLP_DAYAHEAD_LOWER_BOUNDS = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=32, no_hidden_layers=1, batch_size=300, no_epochs=700)
]

MLP_DAYAHEAD_UPPER_BOUNDS_POZNAN = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=40, no_hidden_layers=1, batch_size=300, no_epochs=600)
]
MLP_DAYAHEAD_LOWER_BOUNDS_POZNAN = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=32, no_hidden_layers=1, batch_size=300, no_epochs=700)
]
