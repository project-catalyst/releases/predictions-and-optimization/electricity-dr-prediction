from abc import ABC, abstractmethod


class PredictionModel(ABC):

    def __init__(self, no_inputs, no_outputs):
        self.no_inputs = no_inputs
        self.no_outputs = no_outputs

    # Defines the used prediction model.
    @abstractmethod
    def define_model(self, prediction_type, data_scenario):
        pass

    # Trains the model using the given data.
    # The data should be DataFrame, one row should be in the format [input_features, expected_output]
    @abstractmethod
    def train_model(self, training_data, prediction_type, data_scenario):
        pass

    # Performs a prediction on the given input data and returns the results as a list of
    # real numbers.
    @abstractmethod
    def predict(self, data):
        pass

    # Saves the model to files at the given path. The path should consist only of folders up to
    # the place where you want the files to be saved.
    # The path must be valid, i.e. all the folders in the path must exist beforehand.
    @abstractmethod
    def save_model(self, path_to_destination, topology_component):
        pass

    # Loads the model from file from the given path. The path should consist only of folders
    # and be up to the files, not including any file itself.
    # Returns True if the model was successfully loaded, and False otherwise.
    @abstractmethod
    def load_model(self, path_to_model_files):
        pass

    # Loads a single model from file from the given path. The path should consist only of folders
    # and be up to the files, not including any file itself.
    # Returns True if the model was successfully loaded, and False otherwise.
    @abstractmethod
    def load_single_model(self, path_to_model_files, model_no):
        pass
