from keras.layers import Dense
from keras.models import Sequential, load_model
from numpy.random import seed
from sklearn.externals import joblib
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import src.algos.prediction_model as pm
from src.algos.constants import constants
import src.algos.mappings as mapping


class MLP(pm.PredictionModel):

    def __init__(self, config):
        super(MLP, self).__init__(config[0].no_inputs, config[0].no_outputs)
        self.config = config
        self.no_models = None
        self.prediction_type = None
        self.model = None
        self.input_scaler = None
        self.output_scaler = None
        seed(1)

    def define_model(self, prediction_type, data_scenario):
        self.no_models = mapping.get_no_models_for_algorithm(
            constants.ALGORITHM_TYPE_MLP, prediction_type, data_scenario)
        self.prediction_type = prediction_type

        self.model = [self.create_model(i) for i in range(self.no_models)]
        self.input_scaler = [MinMaxScaler(feature_range=(-1, 1)) for _ in range(self.no_models)]
        self.output_scaler = [MinMaxScaler(feature_range=(-1, 1)) for _ in range(self.no_models)]

        return self.model

    def train_model(self, train_data, prediction_type, data_scenario):
        self.define_model(prediction_type, data_scenario)

        histories = []

        # format data
        xs, ys = self.format_data(train_data, self.prediction_type)

        for i in range(self.no_models):
            x, y = xs[i], ys[i]

            x = self.input_scaler[i].fit_transform(x)
            y = self.output_scaler[i].fit_transform(y)

            history = self.model[i].fit(x, y, epochs=self.config[i].no_epochs, batch_size=self.config[i].batch_size,
                                        verbose=True, validation_split=0.1)
            histories.append(history)

        return histories

    def predict(self, input_data):
        predictions = []
        if isinstance(input_data, pd.DataFrame):
            input_data = input_data.values
        xs, ys = self.format_data(input_data, self.prediction_type)

        for i in range(self.no_models):
            x, y = xs[i], ys[i]
            x = self.input_scaler[i].transform(x)

            prediction = self.model[i].predict(x, verbose=1)
            prediction = self.output_scaler[i].inverse_transform(prediction)

            predictions.append(prediction)

        return predictions, ys

    def save_model(self, path_to_destination, topology_component):
        for i in range(self.no_models):
            self.model[i].save("%s/%s/mlp_model_%d.h5" % (path_to_destination, topology_component, i))

            # Save the scalers
            joblib.dump(self.input_scaler[i],
                        "%s/%s/mlp_model_input_scaler_%d.pkl" % (path_to_destination, topology_component, i))
            joblib.dump(self.output_scaler[i],
                        "%s/%s/mlp_model_output_scaler_%d.pkl" % (path_to_destination, topology_component, i))

    def load_model(self, path_to_model_files):
        self.model, self.input_scaler, self.output_scaler = [], [], []

        try:
            for i in range(self.no_models):
                self.input_scaler.append(joblib.load("%s/mlp_model_input_scaler_%d.pkl" % (path_to_model_files, i)))
                self.output_scaler.append(joblib.load("%s/mlp_model_output_scaler_%d.pkl" % (path_to_model_files, i)))

                self.model.append(load_model("%s/mlp_model_%d.h5" % (path_to_model_files, i)))

                print("Model %d has been loaded." % i)

            return True
        except Exception as e:
            print(e)
            return False

    def load_single_model(self, path_to_model_files, part_of_day):
        self.model, self.input_scaler, self.output_scaler = [], [], []

        try:
            self.input_scaler.append(joblib.load("%s/mlp_model_input_scaler_%d.pkl" % (path_to_model_files, part_of_day)))
            self.output_scaler.append(joblib.load("%s/mlp_model_output_scaler_%d.pkl" % (path_to_model_files,
                                                                                         part_of_day)))

            self.model.append(load_model("%s/mlp_model_%d.h5" % (path_to_model_files, part_of_day)))

            return True
        except Exception as e:
            print(e)
            return False

    def create_model(self, i):
        model = Sequential()
        model.add(Dense(self.config[i].no_inputs,
                        kernel_initializer='he_uniform',
                        activation='relu',
                        input_dim=self.config[i].no_inputs))
        for _ in range(self.config[i].no_hidden_layers):
            model.add(Dense(self.config[i].no_neurons,
                            kernel_initializer='he_uniform',
                            activation='relu'))
        model.add(Dense(self.config[i].no_outputs,
                        kernel_initializer='he_uniform'))
        model.compile(loss='mean_squared_error',
                      optimizer='adam',
                      metrics=['accuracy'])

        return model

    def format_data(self, train_data, prediction_type):
        """ Parses data depending on prediction_type:
            Returns data as 2 lists of dataFrames:
                xs: list of inputs for each model
                ys: list of outputs for each model
        """

        # TODO: SEPARATE logic for each scenario if needed; adding data_scenario parameter
        if prediction_type == constants.DAYAHEAD:
            xs, ys = [], []

            xs.append(train_data[:, :self.no_inputs])
            ys.append(train_data[:, self.no_inputs:])

            return xs, ys

        if prediction_type == constants.INTRADAY:
            xs, ys = [], []

            for i in range(self.no_models):
                is_part_of_day = train_data[:, self.config[0].no_inputs] == i
                xs.append(train_data[is_part_of_day, :self.config[0].no_inputs])
                ys.append(train_data[is_part_of_day, (self.config[0].no_inputs + 1):])

            return xs, ys

        if prediction_type == constants.NEAR_REAL_TIME:
            xs, ys = [], []

            xs.append(train_data[:, :self.no_inputs])
            ys.append(train_data[:, self.no_inputs:])

            return xs, ys

    def train_and_predict(self, train_data, prediction_type, data_scenario, test_data):
        self.train_model(train_data, prediction_type, data_scenario)
        predictions = self.predict(test_data)

        return predictions

    def predict_single_model(self, data):
        if isinstance(data, pd.DataFrame):
            data = data.values
        xs, ys = [], []
        xs.append(data[:, :self.no_inputs])
        ys.append(data[:, self.no_inputs:])

        xs = self.input_scaler[0].transform(xs[0])
        prediction = self.model[0].predict(xs, verbose=1)
        prediction = self.output_scaler[0].inverse_transform(prediction)

        return prediction
