import numpy as np


class Metrics:

    def __init__(self):
        pass

    @staticmethod
    def mean_absolute_percentage_error(y_true, y_pred):
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    @staticmethod
    def mean_squared_error(y_true, y_pred):
        return [np.mean(np.square(y_true[i] - y_pred[i])) for i in range(0, len(y_true))]

    @staticmethod
    def root_mean_square_error(y_true, y_pred):
        return np.sqrt(((y_true - y_pred) ** 2).mean())

    @staticmethod
    def mean_absolute_error(y_true, y_pred):
        return np.mean(np.absolute(y_true - y_pred))
