import src.algos.algo_config as config
import src.algos.constants.no_models as no_models
from src.algos.constants import constants


def get_no_models_for_algorithm(algorithm_type, prediction_type, data_scenario):
    switch = {
        # Scenario 0 - Paper
        (
            constants.DAYAHEAD, constants.DATA_SCENARIO[0],
            constants.ALGORITHM_TYPE_MLP): no_models.MLP_NO_MODELS_DAYAHEAD,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0],
         constants.ALGORITHM_TYPE_LSTM): no_models.LSTM_NO_MODELS_DAYAHEAD,

        (
            constants.INTRADAY, constants.DATA_SCENARIO[0],
            constants.ALGORITHM_TYPE_MLP): no_models.MLP_NO_MODELS_INTRADAY,
        (constants.INTRADAY, constants.DATA_SCENARIO[0],
         constants.ALGORITHM_TYPE_LSTM): no_models.LSTM_NO_MODELS_INTRADAY,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP):
            no_models.MLP_NO_MODELS_NEAR_REAL_TIME,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM):
            no_models.LSTM_NO_MODELS_NEAR_REAL_TIME,

        # Scenario 1 - Poznan
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1],
         constants.ALGORITHM_TYPE_MLP): no_models.MLP_NO_MODELS_DAYAHEAD_POZNAN,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1],
         constants.ALGORITHM_TYPE_LSTM): no_models.LSTM_NO_MODELS_DAYAHEAD_POZNAN,

        (constants.INTRADAY, constants.DATA_SCENARIO[1],
         constants.ALGORITHM_TYPE_MLP): no_models.MLP_NO_MODELS_INTRADAY_POZNAN,
        (constants.INTRADAY, constants.DATA_SCENARIO[1],
         constants.ALGORITHM_TYPE_LSTM): no_models.LSTM_NO_MODELS_INTRADAY_POZNAN,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP):
            no_models.MLP_NO_MODELS_NEAR_REAL_TIME_POZNAN,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM):
            no_models.LSTM_NO_MODELS_NEAR_REAL_TIME_POZNAN
    }

    return switch.get((prediction_type, data_scenario, algorithm_type), None)


def get_no_models_for_flexibility_algorithm(algorithm_type, prediction_type, flexibility_type):
    switch = {
        (constants.DAYAHEAD, constants.ALGORITHM_TYPE_MLP,
         constants.FLEXIBILITY_TYPE_UPPER): no_models.MLP_NO_MODELS_UPPER_BOUND,
        (constants.DAYAHEAD, constants.ALGORITHM_TYPE_MLP,
         constants.FLEXIBILITY_TYPE_UPPER): no_models.MLP_NO_MODELS_LOWER_BOUND
    }

    return switch.get((prediction_type, algorithm_type, flexibility_type), None)


def get_flexibility_paths(flexibility_type, data_scenario):
    training_data_path = constants.get_flexibility_training_data_path(data_scenario, flexibility_type)
    trained_models_path = constants.get_flexibility_trained_models_path(data_scenario, flexibility_type)
    test_data_path = constants.get_flexibility_test_data_path(data_scenario, flexibility_type)

    return training_data_path, trained_models_path, test_data_path


def get_config_for_flexibility_algorithm(prediction_type, flexibility_type, data_scenario):
    switch = {
        # Scenario 0 - Paper
        (constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_UPPER,
         constants.DATA_SCENARIO[0]): config.MLP_DAYAHEAD_UPPER_BOUNDS,
        (constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_LOWER,
         constants.DATA_SCENARIO[0]): config.MLP_DAYAHEAD_LOWER_BOUNDS,

        # Scenario 1 - Poznan
        (constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_UPPER,
         constants.DATA_SCENARIO[1]): config.MLP_DAYAHEAD_UPPER_BOUNDS_POZNAN,
        (constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_LOWER,
         constants.DATA_SCENARIO[1]): config.MLP_DAYAHEAD_LOWER_BOUNDS_POZNAN
    }

    return switch.get((prediction_type, flexibility_type, data_scenario), None)


def get_paths(prediction_type, data_scenario):
    training_data_path = constants.get_training_data_path(data_scenario, prediction_type)
    trained_models_path = constants.get_trained_models_path(data_scenario, prediction_type)
    test_data_path = constants.get_test_data_path(data_scenario, prediction_type)

    return training_data_path, trained_models_path, test_data_path


def get_config_for_algorithms(prediction_type, data_scenario, topology_component):
    switch = {
        # Scenario 0 - Paper
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT): (
            config.MLP_DAYAHEAD_SERVER, config.LSTM_DAYAHEAD_SERVER),
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_DAYAHEAD_COOLING, config.LSTM_DAYAHEAD_COOLING),

        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT): (
            config.MLP_INTRADAY_SERVER, config.LSTM_INTRADAY_SERVER),
        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_INTRADAY_COOLING, config.LSTM_INTRADAY_COOLING),

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT): (
            config.MLP_NEAR_REAL_TIME_SERVER, config.LSTM_NEAR_REAL_TIME_SERVER),
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_NEAR_REAL_TIME_COOLING, config.LSTM_NEAR_REAL_TIME_COOLING),

        # Scenario 1 - Poznan
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT): (
            config.MLP_DAYAHEAD_SERVER_POZNAN, config.LSTM_DAYAHEAD_SERVER_POZNAN),
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_DAYAHEAD_COOLING_POZNAN, config.LSTM_DAYAHEAD_COOLING_POZNAN),

        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT): (
            config.MLP_INTRADAY_SERVER_POZNAN, config.LSTM_INTRADAY_SERVER_POZNAN),
        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_INTRADAY_COOLING_POZNAN, config.LSTM_INTRADAY_COOLING_POZNAN),

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT): (
            config.MLP_NEAR_REAL_TIME_SERVER_POZNAN, config.LSTM_NEAR_REAL_TIME_SERVER_POZNAN),
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.COOLING_SYSTEM_COMPONENT): (
            config.MLP_NEAR_REAL_TIME_COOLING_POZNAN, config.LSTM_NEAR_REAL_TIME_COOLING_POZNAN)
    }

    return switch.get((prediction_type, data_scenario, topology_component), None)


def get_model_config(algorithm_type, prediction_type, data_scenario, topology_component):
    switch = {
        # ### Scenario 0 - Paper
        # Server
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP, constants.SERVER_COMPONENT):
            config.MLP_DAYAHEAD_SERVER,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM, constants.SERVER_COMPONENT):
            config.LSTM_DAYAHEAD_SERVER,

        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP, constants.SERVER_COMPONENT):
            config.MLP_INTRADAY_SERVER,
        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM, constants.SERVER_COMPONENT):
            config.LSTM_INTRADAY_SERVER,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP,
         constants.SERVER_COMPONENT): config.MLP_NEAR_REAL_TIME_SERVER,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM,
         constants.SERVER_COMPONENT): config.LSTM_NEAR_REAL_TIME_SERVER,

        # Cooling
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_DAYAHEAD_COOLING,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_DAYAHEAD_COOLING,

        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_INTRADAY_COOLING,
        (constants.INTRADAY, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_INTRADAY_COOLING,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_NEAR_REAL_TIME_COOLING,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_NEAR_REAL_TIME_COOLING,

        # ### Scenario 1 - Poznan
        # Server
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP, constants.SERVER_COMPONENT):
            config.MLP_DAYAHEAD_SERVER_POZNAN,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM, constants.SERVER_COMPONENT):
            config.LSTM_DAYAHEAD_SERVER_POZNAN,

        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP, constants.SERVER_COMPONENT):
            config.MLP_INTRADAY_SERVER_POZNAN,
        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM, constants.SERVER_COMPONENT):
            config.LSTM_INTRADAY_SERVER_POZNAN,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP,
         constants.SERVER_COMPONENT): config.MLP_NEAR_REAL_TIME_SERVER_POZNAN,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM,
         constants.SERVER_COMPONENT): config.LSTM_NEAR_REAL_TIME_SERVER_POZNAN,

        # Cooling
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_DAYAHEAD_COOLING_POZNAN,
        (constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_DAYAHEAD_COOLING_POZNAN,

        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_INTRADAY_COOLING_POZNAN,
        (constants.INTRADAY, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_INTRADAY_COOLING_POZNAN,

        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_MLP,
         constants.COOLING_SYSTEM_COMPONENT): config.MLP_NEAR_REAL_TIME_COOLING_POZNAN,
        (constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.ALGORITHM_TYPE_LSTM,
         constants.COOLING_SYSTEM_COMPONENT): config.LSTM_NEAR_REAL_TIME_COOLING_POZNAN
    }

    return switch.get((prediction_type, data_scenario, algorithm_type, topology_component), None)
