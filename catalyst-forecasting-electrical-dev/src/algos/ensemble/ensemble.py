import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import src.algos.utils as utils
from src.algos.constants import constants


class Ensemble:

    @staticmethod
    def ensemble_prediction(data, algorithm_models, trained_models_root_path, topology_component):
        predictions, y = list(), None

        for alg_type, alg_model in algorithm_models.items():
            if alg_model is not None:
                prediction, y = utils.predict(data, alg_type, alg_model, trained_models_root_path, topology_component)
                if alg_type == 'mlp':
                    predictions_mlp = utils.predict(data, alg_type, alg_model, trained_models_root_path, topology_component)
                if alg_type == 'lstm':
                    predictions_lstm = utils.predict(data, alg_type, alg_model, trained_models_root_path, topology_component)
                predictions.append(prediction)

        predictions = np.array(predictions)

        # call for weighted average
        ensemble_predictions, weight = Ensemble.get_best_prediction(predictions, y)
        # print("Weights for mlp's model: %s" % str(weight))

        #ensemble_predictions = predictions.mean(axis=0)
        ensemble_predictions = list(ensemble_predictions)

        predictions_mlp = pd.DataFrame(predictions_mlp[0][0].reshape(-1,1))
        predictions_lstm = pd.DataFrame(predictions_lstm[0][0].reshape(-1, 1))
        expected_data = y[0].reshape(-1,1)
        predictions_ensemble = pd.DataFrame(ensemble_predictions[0].reshape(-1,1))
        fig = plt.figure()
        plt.plot(predictions_mlp, label='mlp')
        plt.plot(predictions_lstm, label='lstm')
        plt.plot(expected_data, label='real')
        plt.plot(predictions_ensemble, label='ensemble')
        plt.legend(loc='upper right')
        plt.title("Prediction results")
        plt.ylabel('Energy [kWh]')
        plt.xlabel('Time [h]')
        plt.ylim(top=900)
        plt.show()
        plt.savefig("prediction_results.png")
        plt.clf()
        plt.cla()

        return ensemble_predictions, y

    @staticmethod
    def single_sample_ensemble_prediction(sample, algorithm_models, trained_models_root_path, topology_component):
        predictions = list()

        for alg_type, alg_model in algorithm_models.items():
            if alg_model is not None:
                prediction, _ = utils.predict(sample, alg_type, alg_model, trained_models_root_path, topology_component)

                predictions.append(prediction)

        predictions = np.array(predictions)
        prediction_length = predictions.shape[-1]
        predictions = predictions.reshape((2, prediction_length))
        ensemble_prediction = predictions.mean(axis=0)
        ensemble_prediction = ensemble_prediction.tolist()

        return ensemble_prediction

    @staticmethod
    def single_sample_intraday_prediction(data, algorithm_models, trained_models_root_path, topology_component, model_no):
        predictions, y = list(), None

        for alg_type, alg_model in algorithm_models.items():
            if alg_model is not None:
                print("Starting prediction using the model %s." % alg_type)

                trained_models_path = "%s/%s/%s" % (trained_models_root_path, alg_type, topology_component)

                with keras.backend.get_session().graph.as_default():
                    alg_model.load_single_model(trained_models_path, model_no)
                    print('Done loading the model.')

                    prediction, _ = alg_model.predict(data)
                    print('Done predicting the data.')

                predictions.append(prediction)

        predictions = np.array(predictions)

        predictions = np.array(predictions).reshape((2, 8))
        ensemble_predictions = predictions.mean(axis=0)
        ensemble_predictions = ensemble_predictions.tolist()

        return ensemble_predictions

    @staticmethod
    def get_best_prediction(predictions, expected_data):
        """ Predictions is a 4-D array: (no_algorithms, no_models, no_samples, no_output_variables)
            Returns best weighted average prediction for each sample.
        """

        mlp_predictions, lstm_predictions = predictions[0], predictions[1]
        ens_predictions, rs, r_model = list(), list(), None

        # iterate over models
        for i in range(predictions.shape[1]):
            r_model = 0.1
            best_mape_prediction = mlp_predictions[i, :, :] * r_model + lstm_predictions[i, :, :] * (1 - r_model)

            for r in np.arange(0.1, 1.0, 0.005):
                temp = mlp_predictions[i, :, :] * r + lstm_predictions[i, :, :] * (1 - r)

                if Ensemble.get_mape_over_samples(best_mape_prediction, expected_data[i]) > \
                        Ensemble.get_mape_over_samples(temp, expected_data[i]):

                    best_mape_prediction = temp
                    r_model = r

            ens_predictions.append(best_mape_prediction)
            rs.append(r_model)

        return ens_predictions, rs

    @staticmethod
    def get_mape_over_samples(prediction, expected_data):
        return 100 * np.average(np.absolute(expected_data - prediction) / expected_data)
