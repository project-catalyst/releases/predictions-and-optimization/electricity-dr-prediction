import numpy as np
import pandas as pd


def series_to_supervised_dayahead(series_data, n_in=24, n_out=24, step=24):
    """ Transforms series data to samples with the form (n_in values, isWeekend, n_out values).
            Each sample begins {step} values after the one before.

            n_in=24, n_out=24, step=24 => simulates the dayahead scenario
                                            input = a day (24 hours) + isWeekend,
                                            output = next day (24 hours)
        """

    # initialize column names
    cols = list()
    cols[:n_in] = ['E(t-%d)' % i for i in range(n_in, 0, -1)]
    # cols.append('isWeekend')
    cols.append('E(t)')
    cols.extend(['E(t+%d)' % i for i in range(1, n_out, )])

    # get each index for day start
    series_data = series_data.reshape((-1, 1))
    data_size = series_data.shape[0] - n_in - n_out + 1
    start_indexes = range(0, data_size, step)
    output = np.zeros(shape=(len(start_indexes), n_in + n_out))

    for i, j in enumerate(start_indexes):
        output[i, :n_in] = series_data[j:j + n_in].flatten()
        output[i, -n_out:] = series_data[j + n_in:j + n_in + n_out].flatten()

    return pd.DataFrame(output, columns=cols)


def series_to_supervised_intraday(series_data, n_in=8, n_out=8, step=8):
    """ Transforms series data to samples with the form (n_in values, n_out values).
               Each sample begins {step} values after the one before.

               n_in=8, n_out=8, step=8 => simulates the intraday scenario
                                               input = a part of day (8 values with 30 min granularity),
                                               output = next part of the day (next 8 values with 30 min granularity)
           """
    # initialize column names
    cols = list()
    cols[:n_in] = ['E(t-%d)' % i for i in range(n_in, 0, -1)]
    # cols.append('isWeekend')
    cols.append('E(t)')
    cols.extend(['E(t+%d)' % i for i in range(1, n_out, )])

    # get each index for day start
    data_size = series_data.shape[0] - n_in - n_out + 1
    start_indexes = range(0, data_size, step)
    output = np.zeros(shape=(len(start_indexes), n_in + n_out))

    for i, j in enumerate(start_indexes):
        output[i, :n_in] = series_data[j:j + n_in].flatten()
        output[i, -n_out:] = series_data[j + n_in:j + n_in + n_out].flatten()

    # TODO group data by part of day and return a list of dataframes (6)

    return pd.DataFrame(output, columns=cols)


def is_weekend_day(timestamp):
    """ Returns 0 if the timestamp day is a week day and 1 otherwise.
    """
    return timestamp.dayofweek // 5


def which_part_of_day(timestamp, part_dim_hours=4):
    """ Divides a day in multiple parts, each containing {part_dim_hours} hours and returns the part corresponding
        to the timestamp.
    """
    return timestamp.hour // part_dim_hours

switcher = {
        0: 1,
        1: 1,
        2: 1,
        3: 1,
        4: 2,
        5: 2,
        6: 2,
        7: 2,
        8: 3,
        9: 3,
        10: 3,
        11: 3,
        12: 4,
        13: 4,
        15: 4,
        16: 5,
        17: 5,
        18: 5,
        19: 5,
        20: 6,
        21: 6,
        22: 6,
        23: 6
    }

def series_to_supervised_intraday_part_of_day(data, n_in=8, n_out=8, step=8, is_weekend=False):
    """ Transforms Series data to samples with the form (n_in values, part_of_day, n_out values).
        Each sample begins {step} values after the one before.

        data = Series type

        n_in=8, n_out=8, step=8 => simulates the intraday scenario
                                        input = 4 hours (8 values) + part_of_day,
                                        output = 4 hours (8 values)
    """
    # initialize column names
    cols = list()
    cols[:n_in] = ['E(t-%d)' % i for i in range(n_in, 0, -1)]
    cols.append('partOfDay')
    cols.append('E(t)')
    cols.extend(['E(t+%d)' % i for i in range(1, n_out, )])

    data_size = data.shape[0] - 2 * step + 1
    start_indexes = range(0, data_size, step)
    output = np.zeros(shape=(len(start_indexes), n_in + 1 + n_out))

    for i, j in enumerate(start_indexes):
        output[i, :n_in] = data[j:j+step].ravel()
        #output[i, n_in] = which_part_of_day(data[j])
        output[i, n_in] = i%6 # as we have 6 different models
        output[i, n_in+1:] = data[j+step:j+2*step].ravel()

    if is_weekend:
        output_is_weekend = np.zeros(shape=(len(start_indexes), 1))
        for i, j in enumerate(start_indexes):
            output_is_weekend[i, 0] = is_weekend_day(data.index[j])
        output = np.insert(output, [n_in + 1], output_is_weekend, axis=1)
        cols = np.insert(cols, n_in + 1, 'isWeekend')

    return pd.DataFrame(output, columns=cols)


def series_to_supervised_near_real_time(series_data, n_in=24, n_out=1, step=1):
    """ Transforms series data to samples with the form (n_in values, n_out values).
                   Each sample begins {step} values after the one before.

                   n_in=24, n_out=1, step=1 => simulates the near real time scenario
                                                   input = last 24 hours values,
                                                   output = next hour value
               """
    # initialize column names
    cols = list()
    cols[:n_in] = ['E(t-%d)' % i for i in range(n_in, 0, -1)]
    # cols.append('isWeekend')
    cols.append('E(t)')
    cols.extend(['E(t+%d)' % i for i in range(1, n_out, )])

    # get each index for day start
    data_size = series_data.shape[0] - n_in - n_out + 1
    start_indexes = range(0, data_size, step)
    output = np.empty(shape=(len(start_indexes), n_in + n_out))

    for i, j in enumerate(start_indexes):
        output[i, :n_in] = series_data[j:j + n_in].flatten()
        output[i, -n_out:] = series_data[j + n_in:j + n_in + n_out].flatten()

    return pd.DataFrame(output, columns=cols)


def split_train_test(data, no_of_samples_for_test):
    train_data = data[:data.shape[0] - no_of_samples_for_test]
    test_data = data[data.shape[0] - no_of_samples_for_test:]

    return train_data, test_data
