import keras
import matplotlib.pyplot as plt
import numpy as np

import src.algos.constants as constants
from src.algos import metrics


# predictions
def predict(data, algorithm_type, algorithm_model, trained_models_root_path, topology_component):
    print("Starting prediction using the model %s." % algorithm_type)

    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

    with keras.backend.get_session().graph.as_default():
        algorithm_model.load_model(trained_models_path)
        print('Done loading the model.')

        predictions, ys = algorithm_model.predict(data)
        print('Done predicting the data.')

    return predictions, ys


def predict_flexibility(data, algorithm_type, algorithm_model, topology_component, flexibility_type):
    switch = {
        constants.FLEXIBILITY_TYPE_LOWER: constants.TRAINED_MODELS_LOWER_BOUND,
        constants.FLEXIBILITY_TYPE_UPPER: constants.TRAINED_MODELS_UPPER_BOUND
    }
    trained_models_root_path = switch.get(flexibility_type)
    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

    with keras.backend.get_session().graph.as_default():
        algorithm_model.load_model(trained_models_path)
        print('Done loading the model.')

        predictions, ys = algorithm_model.predict(data)
        print('Done predicting the data.')

    return predictions, ys


# plots
def plot_prediction(predictions, test_data, prediction_type, topology_component, algorithm_type, flexibility_type=None):
    no_models = len(predictions)
    all_predictions = list()
    all_expected = list()

    for i in range(no_models):
        prediction_per_model = predictions[i]
        expected_data_per_model = test_data[i]
        no_samples = prediction_per_model.shape[0]

        for j in range(no_samples):
            prediction = prediction_per_model[j, :]
            expected_data = expected_data_per_model[j, :]

            title = "%s - %s - %s - model: %d/%d, sample: %d/%d" % \
                    (algorithm_type, prediction_type, topology_component, i + 1, no_models, j + 1, no_samples)

            if flexibility_type is None:
                saving_path = "%s/%s/%s/%s/result_%s_model_%d-outOf-%d_sample_%d-outOf-%d_%s.png" % \
                              (constants.RESULTS_PATH, prediction_type, topology_component, algorithm_type,
                               algorithm_type, i + 1, no_models, j + 1, no_samples, topology_component)
            else:
                saving_path = "%s/%s/results_%s_%s.png" % \
                              (constants.FLEXIBILITY_RESULTS_PATH, flexibility_type, algorithm_type, topology_component)

            plt.figure()
            plt.title(title)

            y_low, y_high = get_plot_limits(prediction_type, flexibility_type)
            plt.ylim(y_low, y_high)

            all_predictions.append(prediction)
            all_expected.append(expected_data)

            if prediction_type == constants.NEAR_REAL_TIME:
                plt.plot(prediction, 'b+')
                plt.plot(expected_data, 'r+')
            else:
                plt.plot(prediction)
                plt.plot(expected_data)

            plt.legend(['predicted', 'expected'], loc='upper left')

            if flexibility_type is None:
                mape = metrics.Metrics.mean_absolute_percentage_error(expected_data, prediction)
                mae = metrics.Metrics.mean_absolute_error(expected_data, prediction)

                plt.figtext(.6, .8, "MAPE = %.5f\nMAE = %.5f" % (mape, mae))
            else:
                if flexibility_type == 'UPPER_BOUND':
                    print('Save upper bounds')
                    # np.savetxt('expected_upper.csv', expected_data, delimiter=',')
                    # np.savetxt('predicted_upper.csv', prediction, delimiter=',')
                elif flexibility_type == 'LOWER_BOUND':
                    print('Save lower bounds')
                    # np.savetxt('expected_lower.csv', expected_data, delimiter=',')
                    # np.savetxt('predicted_lower.csv', prediction, delimiter=',')
                rmse = metrics.Metrics.root_mean_square_error(expected_data, prediction)
                plt.figtext(.6, .8, "RMSE = %.5f" % rmse)
            plt.savefig(saving_path)
            plt.draw()

            plt.clf()
            plt.cla()
            plt.close()

    # np.savetxt("predictions_nrt.csv", all_predictions, delimiter=",")
    # np.savetxt("expected_nrt.csv", all_expected, delimiter=",")


def get_plot_limits(prediction_type, flexibility_type):
    if flexibility_type is None:
        return 0.0, 3100.0

    elif prediction_type == constants.DAYAHEAD:
        if flexibility_type == constants.UPPER_BOUND:
            return 0.0, 1000.0

        elif flexibility_type == constants.LOWER_BOUND:
            return -1000.0, 0.0


def plot_loss(histories, algorithm_type, prediction_type, topology_component, flexibility_type=None):
    histories_max_index = len(histories)

    for i, history in enumerate(histories):
        title = "%s - %s - %s - Model %d/%d" % \
                (topology_component, algorithm_type, prediction_type, i + 1, histories_max_index)

        if flexibility_type is None:
            save_path = "%s/%s/%s/loss_%s_sample_%d-outOf-%d.png" % \
                        (constants.LOSS_PATH, prediction_type, topology_component, algorithm_type, i + 1,
                         histories_max_index)
        else:
            save_path = "%s/%s/loss_%s_sample_%d-outOf-%d.png" % \
                        (constants.FLEXIBILITY_LOSS_PATH, flexibility_type, algorithm_type, i + 1, histories_max_index)

        plt.title(title)
        plt.plot(history.history['loss'], label='train')
        plt.plot(history.history['val_loss'], label='test')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper right')
        plt.savefig(save_path)
        plt.draw()

        plt.clf()
        plt.cla()
        plt.close()


# utils
def extract_energy_values(json_energy_profile):
    energy_values = json_energy_profile['curve']
    energy_values = [energy_values[i]['energyValue'] for i in range(len(energy_values))]
    energy_values = np.array(energy_values).reshape(1, -1)

    return energy_values
