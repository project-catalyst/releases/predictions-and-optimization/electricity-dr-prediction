import datetime
import json
import os
import src.algos.utils as utils
from flask import Blueprint, request, Response
import pandas as pd
from src.algos import utils
from src.algos.ensemble.ensemble import Ensemble
from src.algos.lstm import lstm
import src.algos.mappings as mapping
from src.algos.mlp import mlp
from src.algos.utils import *
import src.training_data.series_to_supervised as series_to_supervised
import time

dayahead_predict = Blueprint('dayahead', __name__)

SERVER_BASELINE = np.array(
    [188.3333333, 440.3333333, 547.3333333, 620.8333333, 615.1666667, 691.8333333, 872.3333333, 1139.5, 1176.166667,
     813.3333333, 719.5, 884.6666667, 1047.666667, 944.5, 821.8333333, 846.3333333, 783.6666667, 602.5,
     512, 627.1666667, 562, 382.1666667, 376.8333333, 165.8333333])

SERVER_POZNAN_BASELINE = np.array(
    [609.5714285714286, 609.4285714285714, 603.5714285714286, 598.2857142857143, 591.8571428571429, 583.5714285714286,
     580.5714285714286, 581.8571428571429, 579.7142857142857, 578.5714285714286, 581.2857142857143, 586.2857142857143,
     596.2857142857143, 610.4285714285714, 621.0, 628.571428571428, 633.7142857142857, 634.2857142857143,
     632.5714285714286, 630.8571428571429, 626.7142857142857, 622.0, 619.1428571428571, 515.7142857142857])


@dayahead_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def predict_dayahead(algorithm_type, data_scenario, topology_component, timestamp):
    start = time.time()
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_dayahead(energy_values, algorithm_type, data_scenario, topology_component, timestamp)
    end = time.time()
    print("Elapsed time " + str(end-start) + " seconds")
    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


# for now topology_component will be the server
@dayahead_predict.route('/predict_flexibility_upper_bounds/<algorithm_type>/<data_scenario>/<topology_component>',
                        methods=['POST'])
def predict_dayahead_flexibility_upper_bounds(algorithm_type, data_scenario, topology_component):
    # TODO: algorithm_type is not used. CONSIDER removing parameter and update path.
    # flexibility prediction will always be done with MLP
    algorithm_type = constants.ALGORITHM_TYPE_MLP

    energy_values = extract_energy_values(request.get_json())

    # take only upper bounds of energy values
    upper_bounds = extract_bounds_from_energy_curve(energy_values, constants.FLEXIBILITY_TYPE_UPPER, data_scenario)\
        .reshape(1, -1)

    algo_config = mapping.get_config_for_flexibility_algorithm(constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_UPPER,
                                                               data_scenario)
    algorithm_model = {
        constants.ALGORITHM_TYPE_MLP: mlp.MLP(algo_config)
        # only MLP
    }.get(algorithm_type)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.DAYAHEAD

    predictions, _ = predict_flexibility(data=upper_bounds,
                                         algorithm_type=algorithm_type,
                                         algorithm_model=algorithm_model,
                                         topology_component=topology_component,
                                         flexibility_type=constants.FLEXIBILITY_TYPE_UPPER,
                                         data_scenario=data_scenario)

    predictions = np.ravel(predictions)
    predictions[predictions < 0] = 0
    predictions = predictions.tolist()

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


# for now topology_component will be the server
@dayahead_predict.route('/predict_flexibility_lower_bounds/<algorithm_type>/<data_scenario>/<topology_component>',
                        methods=['POST'])
def predict_dayahead_flexibility_lower_bounds(algorithm_type, data_scenario, topology_component):
    # TODO: algorithm_type is not used. CONSIDER removing parameter and update path.
    # flexibility prediction will always be done with MLP
    algorithm_type = constants.ALGORITHM_TYPE_MLP

    energy_values = extract_energy_values(request.get_json())

    # take only lower bounds of energy values
    lower_bounds = extract_bounds_from_energy_curve(energy_values, constants.FLEXIBILITY_TYPE_LOWER, data_scenario)\
        .reshape(1, -1)

    algo_config = mapping.get_config_for_flexibility_algorithm(constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_LOWER,
                                                               data_scenario)
    algorithm_model = {
        constants.ALGORITHM_TYPE_MLP: mlp.MLP(algo_config)
        # only MLP
    }.get(algorithm_type)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.DAYAHEAD

    predictions, _ = predict_flexibility(data=lower_bounds,
                                         algorithm_type=algorithm_type,
                                         algorithm_model=algorithm_model,
                                         topology_component=topology_component,
                                         flexibility_type=constants.FLEXIBILITY_TYPE_LOWER,
                                         data_scenario=data_scenario)

    predictions = np.ravel(predictions)
    predictions[predictions > 0] = 0
    predictions = predictions.tolist()

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def extract_bounds_from_energy_curve(energy_values, flexibility_type, data_scenario):
    lower_bounds = np.zeros(24)

    baseline = None
    if data_scenario == constants.DATA_SCENARIO[0]: # Scenario 0 - Paper
        baseline = SERVER_BASELINE
    elif data_scenario == constants.DATA_SCENARIO[1]: # Scenario 1 - Paper
        baseline = SERVER_POZNAN_BASELINE

    energy_values = energy_values.reshape(24)

    diff = energy_values - baseline
    bounds_value_picker = diff < 0 if flexibility_type == constants.FLEXIBILITY_TYPE_LOWER else diff > 0

    lower_bounds[bounds_value_picker] = diff[bounds_value_picker]

    return lower_bounds


def sample_predict_dayahead(sample, algorithm_type, data_scenario, topology_component, timestamp):

    # pre-process sample data based on scenario
    data_scenario = data_scenario.lower()
    if data_scenario == constants.DATA_SCENARIO[0]:  # Paper
        date_time_obj = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M')
        is_weekend = date_time_obj.weekday() // 5
        sample = np.append(sample, is_weekend).reshape(1, -1)
    elif data_scenario == constants.DATA_SCENARIO[1]:  # Poznan
        pass

    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.DAYAHEAD, data_scenario, topology_component)
        algorithm_models = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.DAYAHEAD
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.DAYAHEAD

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.DAYAHEAD)
        predictions = Ensemble.single_sample_ensemble_prediction(sample=sample,
                                                                 algorithm_models=algorithm_models,
                                                                 trained_models_root_path=trained_models_root_path,
                                                                 topology_component=topology_component)
    else:
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.DAYAHEAD, data_scenario, topology_component)
        algorithm_model = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }.get(algorithm_type)
        algorithm_model.no_models = 1
        algorithm_model.prediction_type = constants.DAYAHEAD

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.DAYAHEAD)
        predictions = predict(data=sample,
                              algorithm_type=algorithm_type,
                              algorithm_model=algorithm_model,
                              trained_models_root_path=trained_models_root_path,
                              topology_component=topology_component)

    return predictions


@dayahead_predict.route('/train/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def train_and_predict_dayahead(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    # form supervised training set from energy values
    # will transform the energy_values in samples of form(n_in_values, n_out_values)
    training_data = series_to_supervised.series_to_supervised_dayahead(energy_values, n_in=24, n_out=24, step=24)

    # perform training on the data with the desired algorithm
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:

        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.DAYAHEAD, data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.DAYAHEAD
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.DAYAHEAD

        # the two root paths for each model will coincide
        rootPathOfTrainedModelMLP = train_model(constants.ALGORITHM_TYPE_MLP,
                                                    constants.DAYAHEAD,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_MLP),
                                                    training_data,
                                                    topology_component)

        rootPathOfTrainedModelLSTM = train_model(constants.ALGORITHM_TYPE_LSTM,
                                                    constants.DAYAHEAD,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_LSTM),
                                                    training_data,
                                                    topology_component)

    return rootPathOfTrainedModelMLP;


def train_model(algorithm_type, prediction_type, data_scenario, algorithm_model, train_data, topology_component):
    print("Starting training the DAYAHEAD model %s." % algorithm_type)

    history = algorithm_model.train_model(train_data.values, prediction_type, data_scenario)
    print('Done training model, saving to disk...')

    pathOnDisk = "D:/" + data_scenario + "/trained_models/" + prediction_type

    algorithm_model.save_model(pathOnDisk, topology_component)
    print('Saved model to disk. Finished training.')

    return pathOnDisk + "/" + topology_component

@dayahead_predict.route('/load_model_and_predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def load_model_and_predict_dayahead(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)
    predictions = load_model_and_predict_dayahead(energy_values, algorithm_type, data_scenario, topology_component, timestamp)
    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def load_model_and_predict_dayahead(sample, algorithm_type, data_scenario, topology_component, timestamp):

    # load models from disk and predict sample
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.DAYAHEAD,data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.DAYAHEAD
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_LSTM, constants.DAYAHEAD, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.DAYAHEAD

    # load and predict MLP trained model
    predictionsMLP = utils.predict_from_saved_model_on_disk(sample,
                                                                data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_MLP,
                                                                algorithm_models[constants.ALGORITHM_TYPE_MLP],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.DAYAHEAD)
    # load LSTM trained model
    predictionsLSTM = utils.predict_from_saved_model_on_disk(sample, data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_LSTM,
                                                                algorithm_models[constants.ALGORITHM_TYPE_LSTM],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.DAYAHEAD)

    ensemble_predictions =(np.array(predictionsMLP[0][0]) + np.array(predictionsLSTM[0][0])) / 2.0

    return ensemble_predictions[0].tolist()