import datetime
import json
import src.algos.mappings as mapping
from flask import Blueprint, request, Response

from src.algos import utils
from src.algos.lstm import lstm
from src.algos.mappings import get_config_for_algorithms
from src.algos.mlp import mlp
from src.algos.utils import *
from src.algos.ensemble.ensemble import Ensemble
from src.training_data import series_to_supervised

intraday_predict = Blueprint('intraday', __name__)


@intraday_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def predict_intraday(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_intraday(energy_values, algorithm_type, data_scenario, topology_component, timestamp)

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def sample_predict_intraday(sample, algorithm_type, data_scenario, topology_component, timestamp):
    # pre-process sample data based on scenario
    data_scenario = data_scenario.lower()
    part_of_day = None
    if data_scenario == constants.DATA_SCENARIO[0]:  # Scenario 0 - Paper
        date_time_obj = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M')
        part_of_day = date_time_obj.hour // 4
        sample = np.append(sample, 0).reshape(1, -1)
    elif data_scenario == constants.DATA_SCENARIO[1]:  # Scenario 1 - Poznan
        part_of_day = 0
        sample = np.append(sample, 0).reshape(1, -1)

    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = get_config_for_algorithms(constants.INTRADAY, data_scenario, topology_component)
        mlp_config = [mlp_config[part_of_day]]
        lstm_config = [lstm_config[part_of_day]]

        algorithm_models = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.INTRADAY
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.INTRADAY

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.INTRADAY)
        predictions = Ensemble.single_sample_intraday_prediction(data=sample,
                                                                 algorithm_models=algorithm_models,
                                                                 trained_models_root_path=trained_models_root_path,
                                                                 topology_component=topology_component,
                                                                 model_no=part_of_day)
    else:
        mlp_config, lstm_config = get_config_for_algorithms(constants.INTRADAY, data_scenario, topology_component)
        mlp_config = [mlp_config[part_of_day]]
        lstm_config = [lstm_config[part_of_day]]

        algorithm_model = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }.get(algorithm_type)
        algorithm_model.no_models = 1
        algorithm_model.prediction_type = constants.INTRADAY

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.INTRADAY)
        trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

        with keras.backend.get_session().graph.as_default():
            algorithm_model.load_single_model(trained_models_path, part_of_day)
            print('Done loading the model.')

            predictions, _ = algorithm_model.predict(sample)
            print('Done predicting the data.')

    return predictions

@intraday_predict.route('/train/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def train_and_predict_intraday(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json())
    energy_values = energy_values.transpose()
    # form supervised training set from energy values
    # will transform the energy_values in samples of form (n_in values, part_of_day, n_out values)
    training_data = series_to_supervised.series_to_supervised_intraday_part_of_day(energy_values, n_in=8, n_out=8, step=8)

    # perform training on the data with the desired algorithm
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:

        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.INTRADAY, data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_MLP, constants.INTRADAY, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.INTRADAY
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_LSTM, constants.INTRADAY, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.INTRADAY

        # intraday should return 6 trained models
        rootPathOfTrainedModelsMLP = train_model(constants.ALGORITHM_TYPE_MLP,
                                                    constants.INTRADAY,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_MLP),
                                                    training_data,
                                                    topology_component)

        rootPathOfTrainedModelsLSTM = train_model(constants.ALGORITHM_TYPE_LSTM,
                                                    constants.INTRADAY,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_LSTM),
                                                    training_data,
                                                    topology_component)

    return rootPathOfTrainedModelsMLP;


def train_model(algorithm_type, prediction_type, data_scenario, algorithm_model, train_data, topology_component):
    print("Starting training the INTRADAY model %s." % algorithm_type)

    history = algorithm_model.train_model(train_data.values, prediction_type, data_scenario)
    print('Done training models, saving to disk...')

    pathOnDisk = "D:/" + data_scenario + "/trained_models/" + prediction_type

    algorithm_model.save_model(pathOnDisk, topology_component)
    print('Saved model to disk. Finished training.')

    return pathOnDisk + "/" + topology_component

@intraday_predict.route('/load_model_and_predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def load_model_and_predict_intraday(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)
    predictions = load_model_and_predict_intraday(energy_values, algorithm_type, data_scenario, topology_component, timestamp)
    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')

def load_model_and_predict_intraday(sample, algorithm_type, data_scenario, topology_component, timestamp):

    # load models from disk and predict sample
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.INTRADAY,data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1 #mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_MLP, constants.INTRADAY, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.INTRADAY
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1 #mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_LSTM, constants.INTRADAY, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.INTRADAY

    # load and predict MLP trained model
    predictionsMLP = utils.predict_from_saved_model_on_disk_intraday(sample,
                                                                data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_MLP,
                                                                algorithm_models[constants.ALGORITHM_TYPE_MLP],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.INTRADAY,
                                                                timestamp)
    # load LSTM trained model
    predictionsLSTM = utils.predict_from_saved_model_on_disk_intraday(sample, data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_LSTM,
                                                                algorithm_models[constants.ALGORITHM_TYPE_LSTM],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.INTRADAY,
                                                                timestamp)

    ensemble_predictions =(np.array(predictionsMLP[0][0]) + np.array(predictionsLSTM[0][0])) / 2.0

    return ensemble_predictions[0].tolist()