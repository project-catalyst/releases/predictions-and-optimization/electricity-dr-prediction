import json

from flask import Blueprint, Response, request

import src.algos.mappings as mapping
from src.algos import utils
from src.algos.ensemble.ensemble import Ensemble
from src.algos.lstm import lstm
from src.algos.mappings import get_config_for_algorithms
from src.algos.mlp import mlp
from src.algos.utils import *
from src.training_data import series_to_supervised

near_real_time_predict = Blueprint('near_real_time', __name__)


@near_real_time_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>',
                              methods=['POST'])
def predict_near_real_time(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_near_real_time(energy_values, algorithm_type, data_scenario, topology_component)

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def sample_predict_near_real_time(sample, algorithm_type, data_scenario, topology_component):
    data_scenario = data_scenario.lower()

    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = get_config_for_algorithms(constants.NEAR_REAL_TIME, data_scenario, topology_component)
        algorithm_models = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.NEAR_REAL_TIME
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.NEAR_REAL_TIME

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.NEAR_REAL_TIME)
        predictions = Ensemble.single_sample_ensemble_prediction(sample=sample,
                                                                 algorithm_models=algorithm_models,
                                                                 trained_models_root_path=trained_models_root_path,
                                                                 topology_component=topology_component)
    else:
        mlp_config, lstm_config = get_config_for_algorithms(constants.NEAR_REAL_TIME, data_scenario, topology_component)
        algorithm_model = {
            constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),
            constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)
        }.get(algorithm_type)
        algorithm_model.no_models = 1
        algorithm_model.prediction_type = constants.NEAR_REAL_TIME

        trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.NEAR_REAL_TIME)
        predictions = predict(data=sample,
                              algorithm_type=algorithm_type,
                              algorithm_model=algorithm_model,
                              trained_models_root_path=trained_models_root_path,
                              topology_component=topology_component)

    return predictions


@near_real_time_predict.route('/train/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def train_and_predict_near_real_time(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json())
    energy_values = energy_values.transpose()

    # form supervised training set from energy values
    # will transform the energy_values in samples of form (n_in values, n_out values).
    training_data = series_to_supervised.series_to_supervised_near_real_time(energy_values, n_in=24, n_out=1, step=1)

    # perform training on the data with the desired algorithm
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:

        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.NEAR_REAL_TIME, data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.NEAR_REAL_TIME
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = 1
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.NEAR_REAL_TIME

        rootPathOfTrainedModelMLP = train_model(constants.ALGORITHM_TYPE_MLP,
                                                    constants.NEAR_REAL_TIME,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_MLP),
                                                    training_data,
                                                    topology_component)

        rootPathOfTrainedModelLSTM = train_model(constants.ALGORITHM_TYPE_LSTM,
                                                    constants.NEAR_REAL_TIME,
                                                    data_scenario.lower(),
                                                    algorithm_models.get(constants.ALGORITHM_TYPE_LSTM),
                                                    training_data,
                                                    topology_component)

    return rootPathOfTrainedModelMLP;


def train_model(algorithm_type, prediction_type, data_scenario, algorithm_model, train_data, topology_component):
    print("Starting training the NEAR_REAL_TIME model %s." % algorithm_type)

    history = algorithm_model.train_model(train_data.values, prediction_type, data_scenario)
    print('Done training model, saving to disk...')

    pathOnDisk = "D:/" + data_scenario + "/trained_models/" + prediction_type

    algorithm_model.save_model(pathOnDisk, topology_component)
    print('Saved model to disk. Finished training.')

    return pathOnDisk + "/" + topology_component

@near_real_time_predict.route('/load_model_and_predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def load_model_and_predict_near_real_time(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)
    predictions = load_model_and_predict_near_real_time(energy_values, algorithm_type, data_scenario, topology_component, timestamp)
    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def load_model_and_predict_near_real_time(sample, algorithm_type, data_scenario, topology_component, timestamp):

    # load models from disk and predict sample
    if algorithm_type == constants.ALGORITHM_TYPE_ENSEMBLE:
        # setup ensemble
        mlp_config, lstm_config = mapping.get_config_for_algorithms(constants.DAYAHEAD,data_scenario.lower(), topology_component)
        algorithm_models = {constants.ALGORITHM_TYPE_MLP: mlp.MLP(mlp_config),constants.ALGORITHM_TYPE_LSTM: lstm.LSTM(lstm_config)}
        algorithm_models[constants.ALGORITHM_TYPE_MLP].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_MLP, constants.NEAR_REAL_TIME, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_MLP].prediction_type = constants.DAYAHEAD
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].no_models = mapping.get_no_models_for_algorithm(constants.ALGORITHM_TYPE_LSTM, constants.NEAR_REAL_TIME, data_scenario.lower())
        algorithm_models[constants.ALGORITHM_TYPE_LSTM].prediction_type = constants.DAYAHEAD

    # load and predict MLP trained model
    predictionsMLP = utils.predict_from_saved_model_on_disk(sample,
                                                                data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_MLP,
                                                                algorithm_models[constants.ALGORITHM_TYPE_MLP],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.DAYAHEAD)
    # load LSTM trained model
    predictionsLSTM = utils.predict_from_saved_model_on_disk(sample, data_scenario.lower(),
                                                                constants.ALGORITHM_TYPE_LSTM,
                                                                algorithm_models[constants.ALGORITHM_TYPE_LSTM],
                                                                constants.MODELS_DISK_ROOT_PATH,
                                                                topology_component,
                                                                constants.DAYAHEAD)

    ensemble_predictions =(np.array(predictionsMLP[0][0]) + np.array(predictionsLSTM[0][0])) / 2.0

    return ensemble_predictions[0].tolist()