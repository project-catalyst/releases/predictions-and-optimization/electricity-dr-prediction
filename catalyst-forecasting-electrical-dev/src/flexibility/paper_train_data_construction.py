import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas import DataFrame, concat

BASELINE_NO_DAYS = 7
FLEXIBILITY_NO_HOURS = 24


def series_to_supervised(data, col_name, n_in=1, n_out=1, dropnan=True):
    df = DataFrame(data)
    cols, names = list(), list()

    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names.append('%s(t-%d)' % (col_name, i))

    # forecast sequence (t, t+1, ..., t+n-1)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names.append('%s(t)' % col_name)
        else:
            names.append('%s(t+%d)' % (col_name, i))

    agg = concat(cols, axis=1)
    agg.columns = names

    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)

    return agg


def split_to_train_test(data, percentage=0.95):
    n_row = data.shape[0]

    train_last_index = math.floor(n_row * percentage)
    train_set = data[:train_last_index]
    test_set = data[train_last_index:]

    return train_set, test_set


def split_to_train_test_by_no_samples_test(data, no_samples):

    train_set = data[:data.shape[0] - no_samples]
    test_set = data[data.shape[0]- no_samples:]

    return train_set, test_set

# detect baseline by taking the first 7 days (7*24 values)
def detect_baseline(server_values):
    baseline = np.array([0] * FLEXIBILITY_NO_HOURS)
    baseline_length = BASELINE_NO_DAYS * FLEXIBILITY_NO_HOURS - 1
    for i in range(baseline_length):
        print(i % FLEXIBILITY_NO_HOURS)
        print(server_values[i % FLEXIBILITY_NO_HOURS])
        baseline[i % FLEXIBILITY_NO_HOURS] = baseline[i % FLEXIBILITY_NO_HOURS] + server_values[i]
    baseline = [x / BASELINE_NO_DAYS for x in baseline]
    return baseline


def construct_upper_bound_data(baseline, training_data):
    upper_bound_data = np.array([0] * len(training_data))
    for i in range(len(training_data)):
        if training_data[i] - baseline[i % FLEXIBILITY_NO_HOURS] > 0:
            upper_bound_data[i] = training_data[i] - baseline[i % FLEXIBILITY_NO_HOURS]
        else:
            upper_bound_data[i] = 0
    return upper_bound_data


def construct_lower_bound_data(baseline, training_data):
    lower_bound_data = np.array([0] * len(training_data))
    for i in range(len(training_data)):
        if training_data[i] - baseline[i % FLEXIBILITY_NO_HOURS] < 0:
            lower_bound_data[i] = training_data[i] - baseline[i % FLEXIBILITY_NO_HOURS]
        else:
            lower_bound_data[i] = 0
    return lower_bound_data


def plot_data(data, plot_title):
    plt.figure()
    plt.title(plot_title)
    plt.ylim(-2000, 2000)
    plt.plot(data)
    plt.draw()
    plt.show()


def plot_multiple_data(data, baseline, plot_title):
    plt.figure()
    plt.title(plot_title)
    plt.ylim(-2000, 2000)
    plt.plot(data)
    plt.plot(baseline)
    plt.legend(['data', 'baseline'], loc='upper left')
    plt.draw()
    plt.show()


if __name__ == '__main__':

    server_data = pd.read_csv("./server_data/server_one_hour_data_old.csv")
    server_values = server_data['server_energy_averaged'].values
    baseline = detect_baseline(server_values)
    plot_data(baseline, 'baseline')

    training_data = server_values[BASELINE_NO_DAYS*FLEXIBILITY_NO_HOURS:len(server_values)]
    print(training_data)
    plot_data(training_data, 'data')

    upper_bound_data = construct_upper_bound_data(baseline, training_data)
    print(upper_bound_data)
    plot_data(upper_bound_data, 'upper_bounds')
    # save data to csv training file
    upper_bounds_df = pd.DataFrame(upper_bound_data)
    # upper_bounds_df.to_csv("./training_data/upper_bound/train.csv", index=False)

    lower_bound_data = construct_lower_bound_data(baseline, training_data)
    print(lower_bound_data)
    plot_data(lower_bound_data, 'lower_bounds')
    # save data to csv training file
    lower_bounds_df = pd.DataFrame(lower_bound_data)
    # lower_bounds_df.to_csv("./training_data/lower_bound/train.csv", index=False)

    plot_multiple_data(training_data[1:24], baseline, 'baseline_vs_data')

    supervised_upper_bound_data = series_to_supervised(upper_bounds_df, col_name='server_E', n_in=24, n_out=24)
    #train_set_upper, test_set_upper = split_to_train_test(supervised_upper_bound_data)
    train_set_upper, test_set_upper = split_to_train_test_by_no_samples_test(supervised_upper_bound_data, 3*24)  # last 3 days
    train_set_upper.to_csv("train_SERVER_ROOM.csv", index=False)
    test_set_upper.to_csv("test_SERVER_ROOM.csv", index=False)
    plot_data(train_set_upper, 'train_upper')
    plot_data(test_set_upper, 'test_upper')

    supervised_lower_bound_data = series_to_supervised(lower_bounds_df, col_name='server_E', n_in=24, n_out=24)
    train_set_lower, test_set_lower = split_to_train_test(supervised_lower_bound_data)
    train_set_lower.to_csv("train_SERVER_ROOM.csv", index=False)
    test_set_lower.to_csv("test_SERVER_ROOM.csv", index=False)
    plot_data(train_set_lower, 'train_upper')
    plot_data(test_set_lower, 'test_upper')
